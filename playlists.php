<?php

  require_once 'header.php';

  $playlists;  // this array will store every video currently relevant. ( those that are being shown on current page)
  $nrOfMatches; // nr of videos matched.
  $nrOfPlaylistsPerPage = 10; // nr of videos to display on each page.

  if (isset($_POST['searchText'])) {
    session_start();
    $_SESSION['playlistSearch'] = $_POST['searchText'];
  }

  $searchText = "";

  if (isset($_SESSION['playlistSearch']) && $_SESSION['playlistSearch'] != "") {
    $searchText = $_SESSION['playlistSearch'];
  }

  $playlistsData = $videoInterface->getPlaylistsBySearch($searchText, $_GET['page'], $nrOfPlaylistsPerPage);
  $playlists = $playlistsData['playlists'];
  $nrOfMatches = $playlistsData['nrOfPlaylists'];

?>


  <link rel="stylesheet" href="css/global.css">
	<link rel="stylesheet" href="css/playlist.css">

<div>

<div class="container-fluid contentContainer">
  <div id="searchDiv">
    <form class="form-inline" action="playlists.php?page=1" method="post">
      <div class="form-group">
        <label for="searchfield" class="sr-only">Search</label>
        <input type="text" class="form-control" name="searchText" id="searchfield">
      </div>
      <button type="submit" class="btn btn-primary">Search</button>
    </form>
  </div>


  <div class="container-fluid" id="resultsContainer">

  <?php
    foreach ($playlists as $key => $playlist) {
      echo "
    <div class='card'>
      <a href='watch.php?plId=".$playlist['id']."'>
        <div class='card-block'>
        <img class='card-img-top' src='https://fbnewsroomus.files.wordpress.com/2015/04/messenger-video-call-carousel-icon.png?w=960' alt='Card image cap'>
        <div class='cardTitleAndDescrptionDiv'>
          <h5 class='card-title'>".$playlist['title']."</h4>
          <p>".$playlist['description']."</p>
        </div>
      </div>
      </a>
    </div>
    ";
    }

  ?>


  </div>

  <!--Pagenation for search results-->
  <nav aria-label="Page navigation results" id="resultsPagenation">
    <ul class="pagination">
      <?php
        $prevPage = ($_GET['page'] - 1);
        if ($prevPage < 1) {
          $prevPage = 1;
        }
        echo "<li class='page-item'><a class='page-link' href='playlists.php?page=".$prevPage."'>Previous</a></li>";

        $i = 0;
        for ($i; $i < $nrOfMatches / $nrOfPlaylistsPerPage; $i++) {
          echo "<li class='page-item nrLink'><a class='page-link' href='playlists.php?page=".($i + 1)."'>".($i + 1)."</a></li>";
        }
        $nextPage = ($_GET['page'] + 1);
        if ($nextPage > $i) {
          $nextPage = $i;
        }
        echo "<li class='page-item'><a class='page-link' href='playlists.php?page=".$nextPage."'>Next</a></li>";
      ?>
    </ul>
  </nav>

</div>



<script type="text/javascript">

  var currentPage = 1;      // the page we are currently on
  var nrPerPage = 4;        // number of results to be displayed on each page
  var currentSearch = "";     // the search that is currently active

  loadVideos(currentPage, nrPerPage, currentSearch, true);

  // this function changes the number of pagenation items. call this once a new saearch has been done as the number of results will change.
  function updatePageNation(nrOfPlaylists, nrPerPage) {
    $("#resultsPagenation .nrLink").remove();
    //console.log(nrOfvideos / nrPerPage);
    for (i = 0; i < nrOfvideos / nrPerPage; i++) {
      $("#videosPagenation .next").before("<li class='page-item nrLink'><a class='page-link' href='#'>" + (i + 1) + "</a></li>");
    }
    makePageItemActive(currentPage); // make the current page active (hiughlighted)
  }

  // function that creates a card for each video gotten by its parameter. the cards are added to the resultscontainer.
  function displayResults(videos) {
    var resultsContainer = $("#resultsContainer");
    resultsContainer.empty();

    for (i = 0; i < videos['videos'].length; i++) {
      resultsContainer.append(
        "<div class='card'>" + 
          "<a href='watch.php?videoId='" + videos['videos'][i]['id'] + "'>" +
            "<div class='card-block'>" + 
              "<img class='card-img-top' src='https://fbnewsroomus.files.wordpress.com/2015/04/messenger-video-call-carousel-icon.png?w=960' alt='Card image cap'>" +
              "<div class='cardTitleAndDescrptionDiv'>" + "<h5 class='card-title'>" + videos['videos'][i]['title'] + "</h4>" + 
                "<p>" + 
                  videos['videos'][i]['description'] + 
                "</p>" +
              "</div>" + 
            "</div>" + 
          "</a>" +
        "</div>");
    }
  //  updatePageNation(7, nrPerPage);
  }

  // gets the info about all videos that are to be displayed on the current page.
  // if it suceeds we display their info.
  function loadVideos(page, nrPerPage, searchText, updatePagenation = false) {
    $.ajax({
      context: this,
      url: 'ajax.php',
      type: 'POST',
      data: {action: 'GET_VIDEOS_BY_PAGE', page: page, nrPerPage : nrPerPage, searchText : searchText},
      dataType: "json",
    })
    .done(function(data) {
      console.log("success");
      displayResults(data);
      if (updatePagenation) {
        updatePageNation(data['nrOfVideos'], nrPerPage);
      }
    })
    .fail(function() {
      console.log("error");
    })
    .always(function(data) {
      console.log("complete");
    });
  }
  
  $("#videosPagenation .previous").click(function(event) {
    if (currentPage > 1) {
      currentPage -= 1;
      loadVideos(currentPage, nrPerPage, currentSearch);
      makePageItemActive(currentPage);
    }
  });

  $("#videosPagenation .next").click(function(event) {
    if (currentPage < $("#videosPagenation .nrLink").length) {
      currentPage = Number(currentPage) + 1;                      // no clue why js threts currentPage as a string here but nowhere else
      loadVideos(currentPage, nrPerPage, currentSearch);
      makePageItemActive(currentPage);
    }
  });

  // dynamicaly creates click listeners for the pagnation numbered pages.
  // if the clicke page is not currently active we make it active and load in the videos.
  $("#videosPagenation").on('click', '.nrLink', function(event) {
    if (!$(this).hasClass('active')) {
      currentPage = $(this).text();
      loadVideos(currentPage, nrPerPage, currentSearch);
      makePageItemActive(currentPage);
    }
  });

  // clicked the serach button loads the videos for that search
  $("#searchBtn").click(function(event) {
    currentSearch = $("#searchfield").val();
    loadVideos(1, nrPerPage, currentSearch, true);
  });

  // makes clicking enter while in searchfild work the same as clicking the search button
  $('#searchVideosForm').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) { 
      e.preventDefault();
      currentSearch = $("#searchfield").val();
    loadVideos(1, nrPerPage, currentSearch, true);
    }
  });

  function makePageItemActive(pageNr) {
    $("#videosPagenation .nrLink").removeClass('active');
    $("#resultsPagenation .nrLink:contains('" + pageNr + "')").addClass('active');
    console.log(currentPage);
  }

</script>


</div>

