<?php
// install the db for specified server

// change username, password and servername to what you use on your db
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "";

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

echo 'Connected successfully'."<br>";

// Create database
$sql = "CREATE DATABASE " . $dbname;
if ($conn->query($sql) === TRUE) {
    echo "Database created successfully"."<br>";
} else {
    die("Error creating database: " . $conn->error);
}

$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//create tables
$sql = 'CREATE TABLE IF NOT EXISTS persistent_login (
  uid int(11) NOT NULL,
  series varchar(255) NOT NULL,
  token varchar(255) NOT NULL,
  PRIMARY KEY (uid)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';

if ($conn->query($sql) === TRUE) {
    echo "Table MyGuests created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$sql = 'CREATE TABLE IF NOT EXISTS users (
  uId int(11) NOT NULL AUTO_INCREMENT,
  email varchar(255) NOT NULL,
  pwd varchar(255) NOT NULL,
  firstName varchar(255) NOT NULL,
  lastName varchar(255) NOT NULL,
  admin tinyint(1) DEFAULT NULL,
  tlf int(8) NOT NULL,
  PRIMARY KEY (uId)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;';

if ($conn->query($sql) === TRUE) {
    echo "Table users created successfully"."<br>";
} else {
    echo "Error creating table: " . $conn->error;
}

$sql = 'INSERT INTO `users` (`uid`, `email`, `pwd`, `firstName`, `lastName`, `tlf`, `admin`) VALUES
(0, "admin@gmail.com", "$2y$10$EuQbgKX1E6ZBW.dNsxbCW.wWGP.hbrGG7wAW2IPsoOHzYsNxk9taG", "admin", "adminsen", 12345678, 1)';

if ($conn->query($sql) === TRUE) {
    echo "admin user created successfully"."<br>";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$sql = 'CREATE TABLE IF NOT EXISTS videos (
  id int(11) NOT NULL AUTO_INCREMENT,
  owner int(11) NOT NULL,
  filePath varchar(255) NOT NULL,
  title varchar(255) NOT NULL,
  description text NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;';

if ($conn->query($sql) === TRUE) {
    echo "Table videos created successfully"."<br>";
} else {
    echo "Error creating table: " . $conn->error;
}

$sql = 'CREATE TABLE IF NOT EXISTS `img_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `videoId` int(11) NOT NULL,
  `url` text NOT NULL,
  `startTime` int(11) NOT NULL,
  `endTime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;';
if ($conn->query($sql) === TRUE) {
    echo "Table img_data created successfully"."<br>";
} else {
    echo "Error creating table: " . $conn->error;
}


$sql = 'CREATE TABLE IF NOT EXISTS `subtitles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filePath` varchar(255) NOT NULL,
  `videoId` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `languageCode` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;';

if ($conn->query($sql) === TRUE) {
    echo "Table subtitles created successfully"."<br>";
} else {
    echo "Error creating table: " . $conn->error;
}


$sql = 'CREATE TABLE IF NOT EXISTS `student_list` (
  `userId` int(11) NOT NULL,
  `videoId` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';
if ($conn->query($sql) === TRUE) {
    echo "Table student_list created successfully"."<br>";
} else {
    echo "Error creating table: " . $conn->error;
}



$sql = 'CREATE TABLE IF NOT EXISTS `ratings` (
  `videoid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';

if ($conn->query($sql) === TRUE) {
    echo "Table ratings created successfully"."<br>";
} else {
    echo "Error creating table: " . $conn->error;
}

$sql = 'CREATE TABLE IF NOT EXISTS `playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;';

if ($conn->query($sql) === TRUE) {
    echo "Table playlists created successfully"."<br>";
} else {
    echo "Error creating table: " . $conn->error;
}

$sql = 'CREATE TABLE IF NOT EXISTS `playlistentries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlistid` int(11) NOT NULL,
  `videoid` int(11) NOT NULL,
  `nrinlist` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;';

if ($conn->query($sql) === TRUE) {
    echo "Table playlistentries created successfully"."<br>"."You can now use the database"."<br>";
    echo "email and password for admin can be found in README.md";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>
