<?php

session_start();
require_once 'include/db.php';		// Connect to the database
require_once 'classes/user.php';	// Do login stuff


$pageTitle = "Add user";

?>
<style media="screen">
	.container {
		width: 70%;
		margin: auto;
		padding-top: 15px;
		margin-bottom: 10%;
	}
</style>

<div class="container">
<?php
// checkes if the user has admin rights
if($user->getAdmin() == 1) {
	if (isset($_POST['emailAdd']) && isset($_POST['passwordAdd']) && isset($_POST['firstNameAdd']) && isset($_POST['lastNameAdd']) && isset($_POST['tlfAdd'])) {	// Create new user
		if(!empty($_POST['emailAdd']) && !empty($_POST['passwordAdd']) && !empty($_POST['firstNameAdd']) && !empty($_POST['lastNameAdd']) && !empty($_POST['tlfAdd'])) {
			$res = $user->addUser($_POST['emailAdd'], $_POST['passwordAdd'], $_POST['firstNameAdd'], $_POST['lastNameAdd'], $_POST['tlfAdd'], $_POST['adminAdd']);
			if (isset($res['success'])) { // alert success message
				echo '<div class="alert alert-success" role="alert"><span class="glyphicon glyphicon-check" aria-hidden="true"></span><span class="sr-only">Success:</span> User added</div>';
			}
			else echo '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> User alreddy exist</div>';
		}
		else echo '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> There cant be any blank text fields</div>';
	}
}
else if(!isset($_SESSION['uid'])) { // checks if the user has a uid, is the user logged in
	echo '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> No user logged inn</div>';
}
else {
	echo '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> User most be admin to add users</div>';
}
//shows the user form for adding users
echo $user->insertUserForm();
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<?php
