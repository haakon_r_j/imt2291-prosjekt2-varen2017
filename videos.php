
<style type="text/css" media="screen">
	.btn-group {
			margin-bottom: 20px;
	}

	.btn-group .active {
		background-color: rgb(2, 117, 216) !important;
	}
</style>

<link rel="stylesheet" href="css/global.css">
<link rel="stylesheet" href="css/videos.css">

<div class="container-fluid contentContainer">

	<div class="btn-group" role="group" aria-label="Basic example" id="buttonSelectGroup">
	  <button type="button" class="btn btn-secondary active">Videos</button>
	  <button type="button" class="btn btn-secondary">Playlists</button>
	</div>

	<!--search form (does not in any way reload page)-->
	<div id="searchDiv">
		<form class="form-inline" id="searchVideosForm">
		  <div class="form-group">
		    <label for="searchfield" class="sr-only">Search</label>
		    <input type="text" class="form-control" id="searchfield">
		  </div>
		  <button type="button" class="btn btn-primary" id="searchBtn">Search</button>
		</form>
	</div>

	<!--this div will contain all the search results(videos)-->
	<div class="container-fluid" id="resultsContainer">

	</div>

	<!--Pagenation for search results-->
	<nav aria-label="Page navigation results" id="resultsPagenation">
	  <ul class="pagination" id="videosPagenation">
	  	<li class='page-item previous'>
	  		<a class='page-link' href='#'>Previous</a></li>

	    <li class='page-item next'><a class='page-link' href='#'>Next</a></li>
	  </ul>
	</nav>

</div>



<script type="text/javascript" charset="utf-8" async defer>

	var currentPage = 1;			// the page we are currently on
	var nrPerPage = 4;				// number of results to be displayed on each page
	var currentSearch = "";			// the search that is currently active
	var lastsearchType = "videos";  // stores type of the most recent search    

	loadVideos(currentPage, nrPerPage, currentSearch, true);


	// cliclking on a video / playlist will load watch.php into main element and pass the correct parameters for the clicked item.
	$("#resultsContainer").on('click', 'a', function(event) {
		event.preventDefault();
		if (lastsearchType.toUpperCase() == "VIDEOS") {
			console.log("videos");
			watchVideo($(this).attr('data-id')); 
			// $("#main_element").load('watch.php', { "id" : $(this).attr('data-id'), "isPlaylist": '0' });
		}
		else if (lastsearchType.toUpperCase() == "PLAYLISTS") {
			console.log("playlists");
			watchPlaylist($(this).attr('data-id'));
			//$("#main_element").load('watch.php', { "plId" : $(this).attr('data-id'), "isPlaylist": '1' });
		}
	});


	// this function changes the number of pagenation items. call this once a new saearch has been done as the number of results will change.
	function updatePageNation(nrOfResults, nrPerPage) {
		$("#resultsPagenation .nrLink").remove();
		//console.log(nrOfvideos / nrPerPage);
		for (i = 0; i < nrOfResults / nrPerPage; i++) {
			$("#videosPagenation .next").before("<li class='page-item nrLink'><a class='page-link' href='#'>" + (i + 1) + "</a></li>");
		}
		makePageItemActive(currentPage); // make the current page active (hiughlighted)
	}

	// function that creates a card for each video / playlist gotten by its parameter. the cards are added to the resultscontainer.
	function displayResults(results) {
		var resultsContainer = $("#resultsContainer");
		resultsContainer.empty();

		for (i = 0; i < results['results'].length; i++) {
			resultsContainer.append(
				"<div class='card'>" + 
					"<a href='#' data-id='"+ results['results'][i]['id'] + "'>" +
						"<div class='card-block'>" + 
							"<img class='card-img-top' src='https://fbnewsroomus.files.wordpress.com/2015/04/messenger-video-call-carousel-icon.png?w=960' alt='Card image cap'>" +
							"<div class='cardTitleAndDescrptionDiv'>" + "<h5 class='card-title'>" + results['results'][i]['title'] + "</h4>" + 
								"<p>" + 
									results['results'][i]['description'] + 
								"</p>" +
							"</div>" + 
						"</div>" + 
					"</a>" +
				"</div>");
		}
	//	updatePageNation(7, nrPerPage);
	}

	// gets the info about all videos /playlists that are to be displayed on the current page.
	// if it suceeds we display their info.
	function loadVideos(page, nrPerPage, searchText, updatePagenation = false) {
		var type = $("#buttonSelectGroup .active").text();
		var ajaxCallType = "GET_" + type.toUpperCase() + "_BY_PAGE";
		$.ajax({
			context: this,
			url: 'ajax.php',
			type: 'POST',
			data: {action: ajaxCallType, page: page, nrPerPage : nrPerPage, searchText : searchText},
			dataType: "json",
		})
		.done(function(data) {
			console.log("success");
			lastsearchType = type;
			displayResults(data);
			if (updatePagenation) {
				updatePageNation(data['nrOfResults'], nrPerPage);
			}
		})
		.fail(function() {
			console.log("error");
		})
		.always(function(data) {
			console.log("complete");
		});
	}
	
	$("#videosPagenation .previous").click(function(event) {
		if (currentPage > 1) {
			currentPage -= 1;
			loadVideos(currentPage, nrPerPage, currentSearch);
			makePageItemActive(currentPage);
		}
	});

	$("#videosPagenation .next").click(function(event) {
		if (currentPage < $("#videosPagenation .nrLink").length) {
			currentPage = Number(currentPage) + 1;                      // no clue why js treats currentPage as a string here but nowhere else
			loadVideos(currentPage, nrPerPage, currentSearch);
			makePageItemActive(currentPage);
		}
	});

	// dynamicaly creates click listeners for the pagnation numbered pages.
	// if the clicke page is not currently active we make it active and load in the videos.
	$("#videosPagenation").on('click', '.nrLink', function(event) {
		if (!$(this).hasClass('active')) {
			currentPage = $(this).text();
			loadVideos(currentPage, nrPerPage, currentSearch);
			makePageItemActive(currentPage);
		}
	});

	// clicked the serach button loads the videos / playlists for that search
	$("#searchBtn").click(function(event) {
		currentSearch = $("#searchfield").val();
		currentPage = 1;
		loadVideos(currentPage, nrPerPage, currentSearch, true);
		makePageItemActive(currentPage);
	});

	// makes clicking enter while in searchfild work the same as clicking the search button
	$('#searchVideosForm').on('keyup keypress', function(e) {
	  var keyCode = e.keyCode || e.which;
	  if (keyCode === 13) { 
	    e.preventDefault();
	    currentSearch = $("#searchfield").val();
	    currentPage = 1;
		loadVideos(currentPage, nrPerPage, currentSearch, true);
		makePageItemActive(currentPage);
	  }
	});

	// sets the pagenation for currentpage to active (blue)
	function makePageItemActive(pageNr) {
		$("#videosPagenation .nrLink").removeClass('active');
		$("#resultsPagenation .nrLink:contains('" + pageNr + "')").addClass('active');
	}

	// adds clicklistener so user can choose between searching for videos or playlists
	$("#buttonSelectGroup button").click(function(event) {
		$("#buttonSelectGroup button").removeClass('active');
		$(this).addClass('active');
	});

</script>




