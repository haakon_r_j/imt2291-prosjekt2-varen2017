<?php

  session_start();

  require_once 'include/db.php';    // Connect to the database
  require_once 'classes/user.php';
  require_once 'classes/videoInterface.php';

   if (isset($_POST['action']) && !empty($_POST['action'])) {
   		if ($_POST['action'] == 'DELETE_USER') {
   			$user->deleteUser($_POST['uid']);
   			echo "deleted user";
   		}

      else if ($_POST['action'] == 'DELETE_VIDEOS') {
        foreach ($_POST['ids'] as $id) {
          $videoInterface->deleteVideo($id);
          echo "deleted videos";
        }
      }


      else if ($_POST['action'] == "ADD_STUDENT_TO_WATCHED_LIST") {
          $videoInterface->addStudentToTheWatchedList($_POST['userId'], $_POST['videoId'], $_POST['ownerId']);
          echo "added student to watched list";
      }

      else if($_POST['action'] == 'GET_STUDENT_WATCHED_LIST') {
          print_r (json_encode($videoInterface->getStudentWatchedList($_POST['videoId'])));
      }

      else if ($_POST['action'] == 'DELETE_PLAYLIST') {
          $videoInterface->deletePlaylist($_POST['id']);
          echo "deleted playlist";
      }

      else if($_POST['action'] == 'IMAGE_DELETE') {
          $videoInterface->deleteVideoImages($_POST['imgData']);
          echo "";
      }

      else if($_POST['action'] == 'GET-VIDEO-IMAGE-DATA') {
          print_r (json_encode($videoInterface->getVideoImageData($_POST['videoId'])));
      }

       else if ($_POST['action'] == 'NEW_PLAYLIST') {
          $videoInterface->createPlayList($_POST['uid'], $_POST['title'], $_POST['description']);
          echo "created playlist";
      }


      else if ($_POST['action'] == 'GET_VIDEO_DESCRIPTION') {
         // $videoInterface->getVideoById($id);
          echo $videoInterface->getVideoById($_POST['id'])['description'];

      }

      else if ($_POST['action'] == 'UPDATE_VIDEO_INFO') {
          $videoInterface->updateVideoInfo($_POST['id'], $_POST['title'], $_POST['description']);
          echo "yay";

      }

      else if ($_POST['action'] == 'GET_VIDEOS_IN_PLAYLIST') {
          $videosArray = $videoInterface->getVideosByPlaylistId($_POST['id']);
          echo json_encode($videosArray);
      }

      else if ($_POST['action'] == 'UPDATE_PLAYLIST_VIDEO_ORDER') {
          foreach ($_POST['idsAndPositions'] as $idsAndPositions) {
            $obj = json_decode($idsAndPositions);
            $id;
            $position;
            foreach($obj as $key => $value) {
              if ($key == 'id') {
                $id = $value;
              }
              else if ($key == 'position') {
                $position = $value;
              }
            }
           $videoInterface->updateVideoPositionInPlaylist($_POST['playlistId'], $id, $position);
           echo "success";
          }
      }

      else if ($_POST['action'] == 'UPDATE_PLAYLIST_INFO') {
          $videoInterface->updatePlaylistInfo($_POST['id'], $_POST['title'], $_POST['title']);
          echo "updated playlist";

      }


      else if ($_POST['action'] == 'RATE_VIDEO') {
          $videoInterface->rateVideo($_POST['rating'], $_POST['videoId']);
          echo "hallelujahs";
      }

      else if ($_POST['action'] == 'REMOVE_VIDEO_FROM_PLAYLIST') {
          $videoInterface->removeVideoFromPlaylist($_POST['playlistId'], $_POST['videoId']);
          echo "hei";

      }

      else if ($_POST['action'] == 'RATE_VIDEO') {
          $videoInterface->rateVideo($_POST['rating'], $_POST['videoId']);
          echo "hallelujahs";
      }

      else if ($_POST['action'] == 'GET_TRACK_HTML') {
          $subtitles = $videoInterface->getSubtitlesByVideoId($_POST['videoId']);
          $resultsArray;
          $trackHtml = "<source src='".$videoInterface->getVideoById($_POST['videoId'])['filePath']."' type='video/mp4'>";
          $languageSelectHtml = "";
          foreach ($subtitles as $key => $subtitle) {
            if ($key == 0) {
                $trackHtml.= "<track label='".$subtitle['language']."' kind='subtitles' srclang='".$subtitle['languageCode']."' src='".$subtitle['filePath']."' default>";
                $languageSelectHtml.= "<li class='list-group-item active' data-lang='".$subtitle['languageCode']."'>".$subtitle['language']."</li>";
            }

            else {
               $trackHtml.= "<track label='".$subtitle['language']."' kind='subtitles' srclang='".$subtitle['languageCode']."' src='".$subtitle['filePath']."' default>";
                $languageSelectHtml.= "<li class='list-group-item' data-lang='".$subtitle['languageCode']."'>".$subtitle['language']."</li>";
            }


          }
          echo json_encode(array("trackHtml" => $trackHtml, "languageSelectHtml" => $languageSelectHtml));
      }

      else if ($_POST['action'] == 'GET_VIDEOS_BY_PAGE') {
          $videosData = $videoInterface->getVideosBySearch($_POST['searchText'], $_POST['page'], $_POST['nrPerPage']);
          echo json_encode($videosData);
      }

       else if ($_POST['action'] == 'GET_PLAYLISTS_BY_PAGE') {
          $playlistsData = $videoInterface->getPlaylistsBySearch($_POST['searchText'], $_POST['page'], $_POST['nrPerPage']);
          echo json_encode($playlistsData);
      }

      else if ($_POST['action'] == 'GET_VIDEOS_BY_USER_ID') {
          $videosData = $videoInterface->getVideosByUuserId($_POST['uid']);
          echo json_encode($videosData);
      }

      else if ($_POST['action'] == 'GET_PLAYLISTS_BY_USER_ID') {
          $playlistsData = $videoInterface->getPlaylistsByUserId($_POST['uid']);
          echo json_encode($playlistsData);
      }

      else if($_POST['action'] == 'GET_VIDEO_BY_ID'){
         echo json_encode($videoInterface->getVideoById($_POST['id']));
      }


      else if ($_POST['action'] == 'NEW_VIDEO') {
         // checks if file exist for not makes new file
        echo "y";
      }

      else if ($_POST['action'] == 'DELETE_USER') {
        $user->deleteUser($_POST['uid']);
        echo "deleted user";
      }

      else if ($_POST['action'] == 'ADD_VIDEO_TO_PLAYLIST') {
            $videoInterface->addVideoToPlayList($_POST['playlistId'], $_POST['videoId']);
            echo "added video to playlist";
      }

      else if ($_POST['action'] == 'LOG_OUT') {
            $user->logout();
            echo "loged out";
      }

  		else {
   			echo "failed";
   		}

	}

?>
