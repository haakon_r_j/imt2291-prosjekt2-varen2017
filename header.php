<?php
session_start();
require_once 'include/db.php';    // Connect to the database
require_once 'classes/user.php';
require_once 'classes/videoInterface.php';
?>
<div>
  <div class="container-fluid" id="banner">
    <div class="row">
      <h1>Boyznthehood</h1>
    </div>
  </div>


    <nav class="navbar navbar-inverse bg-inverse navbar-toggleable-md navbar-light bg-faded" id="topNavbar">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>


      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item" data-file="frontpage.php">
              <a class="nav-link" href="#">Home <span class="sr-only"></span></a>
          </li>
            <li class="nav-item" data-file="videos.php">
              <a class="nav-link" href="#">Browse</a>
            </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">


          <?php if (isset($_SESSION['uid'])) {  // checks if the user is logged in and show som extra functionality for the users
            echo "<li class='nav-item'>" .
              "<a class='nav-link' id='signOut' href='#'>Sign out</a>".
          "</li>";
          }
          else {
            echo
          '<li role="presentation" class="dropdown" id="signInDropDownToggle">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                Sign in <span class="caret"></span>
              </a>
              <ul class="dropdown-menu dropdown-menu-right">


                <div class="container-fluid" id="signInDropDown">
                  <form>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-3 col-form-label signIn-label">Email</label>
                      <div class="col-sm-9">
                        <input type="email" class="form-control" name="email" id="inputEmail3" placeholder="Email" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-3 col-form-label signIn-label">Password</label>
                      <div class="col-sm-9">
                        <input type="password" class="form-control" name="password" id="inputPassword3" placeholder="Password" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class=" col-sm-6">
                        <button type="button" id="loginButton" class="btn btn-primary btn-block">Sign in!</button>
                      </div>
                      <div class="col-sm-6 form-check">
                        <label class="form-check-label">
                          <input class="form-check-input" id="remember" name="remember" type="checkbox">Remember me
                        </label>
                    </div>

                  </form>';

                  // if login was not successfull;
                   if(isset($_GET['login'])) {
                    echo
                      '<div class="alert alert-danger" role="alert">
                      <strong>Oh snap!</strong> Change a few things up and try submitting again.
                      </div>';
                   }

                  echo
                '</div>
              </ul>
          </li>';
          }
          ?>
      </ul>
      </div>
  </nav>


  <?php if (isset($_SESSION['uid'])) { // checks if the user is logged inn and if he is an admin, if the user has admin rights he is shown
                                       // some extra functionality that are only visible for admins
            echo '<div class="container-fluid" id="side-navbar">
            <div class="row">
                <div class="col-md-4 col-lg-3">

                    <div class="bootstrap-vertical-nav">

                        <button class="btn btn-primary hidden-md-up" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <span class="">Menu</span>
                        </button>

                        <div class="collapse" id="collapseExample">
                            <ul class="nav flex-column" id="exCollapsingNavbar3">
                                <li class="nav-item" data-file="profile.php">
                                    <a class="nav-link" href="#">My profile</a>
                                </li>
                            ';
                                if ($user->getAdmin()) { // checks for admin rights
                                    echo '
                                    <li class="nav-item" data-file="userList.php">
                                        <a class="nav-link" href="#">List Users</a>
                                     </li>
                                     <li class="nav-item" data-file="addUser.php">
                                        <a class="nav-link" href="#">Add User</a>
                                     </li>
                                    ';
                                }
                                echo
                            '</ul>
                        </div>

                    </div>

                </div>

             </div>
          </div>';

    }
    ?>

</div>


<script type="text/javascript">


  $(document).scroll(function() {
    if ($(document).scrollTop() >= $("#banner").height() && !$("#topNavbar").hasClass('fixed-top')) {
      $("#topNavbar").addClass('fixed-top');
    }
    else if ($(document).scrollTop() < $("#banner").height() && $("#topNavbar").hasClass('fixed-top')) {
      $("#topNavbar").removeClass('fixed-top');
    }
  });


  $('#loginButton').click(function(event) {
    var pwd = $('#inputPassword3').val();
    var email = $('#inputEmail3').val();
    var remember = $('#remember').is(':checked');

    console.log(remember + "me");
    console.log(email + "me");
    console.log(pwd + "me");


    console.log(remember + "me and u");

    $.ajax({
      context: this,
      url: 'index.php',
      data: { password: pwd, email: email, remember: remember},
      type: 'POST',
      success: function(response){

          console.log('success');
          $('#header-div').empty();
          loadHeader();

          var mainElement = $("#main_element");
          mainElement.empty();
          mainElement.load('frontpage.php');

      }, error: function(response) {
          console.log('error');
      }
    });
  });

  $('#signOut').click(function() {

    $.ajax({
      context: this,
      url: 'ajax.php',
      data: { action: "LOG_OUT" },
      type: 'POST',
      success: function(response){
          console.log('success');

          $('#header-div').empty();
          loadHeader();

          var mainElement = $("#main_element");
          mainElement.empty();
          mainElement.load('frontpage.php');

      }, error: function(response) {
          console.log('error');
      }
    });
  });

</script>
