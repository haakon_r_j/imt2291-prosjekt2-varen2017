<?php

//bruker id / video id / img uploads
echo print_r($_POST);

if (!file_exists("uploads/" . $_POST['userId'] . "/" . 'image-uploads-' . $_POST['videoId'] . "/")) {
    mkdir("uploads/" . $_POST['userId'] . "/" . 'image-uploads-' . $_POST['videoId'] . "/", 0777, true);
}

$target_dir = "uploads/" . $_POST['userId'] . "/" . 'image-uploads-' . $_POST['videoId'] . "/";
$temp = explode(".", $_FILES["fileToUpload"]["name"]);
$newfilename  = $_POST["startTime"] . "-" . $_POST["endTime"]. '.' . end($temp);
$target_file = $target_dir . $newfilename;//basename($_FILES["fileToUpload"]["name"]);
$videoId = $_POST['videoId'];


/*$temp = explode(".", $_FILES["file"]["name"]);
$newfilename = round(microtime(true)) . '.' . end($temp);
move_uploaded_file($_FILES["file"]["tmp_name"], "../img/imageDirectory/" . $newfilename);*/


$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
else {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      if(storeInDb($videoId, $target_dir . $newfilename, $_POST["startTime"], $_POST["endTime"])) {
        echo "The file ". $newfilename. " has been uploaded.";
      }
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}


//stores image path, the start time and end time,
//will in future have another variable videoId
function storeInDb($videoId, $path, $startTime, $endTime) {
  require_once "include/db.php";


  $sql = "INSERT INTO img_data (videoId, url, startTime, endTime)
  VALUES (?, ?, ?, ?)";

  $sth = $db->prepare ($sql);
  $sth->execute (array ($videoId, $path, $startTime, $endTime));

  if($sth->rowCount()==0) {
    return false;
  }
  else return true;
}
?>
