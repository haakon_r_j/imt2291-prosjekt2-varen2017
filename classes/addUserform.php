<div class="panel-body" >
         <form id="signupform" class="form-horizontal" role="form" method="post" action="addUser.php">

             <div class="form-group">
                 <label for="email" class="col-md-3 control-label">Email</label>
                 <div class="col-md-9">
                     <input type="email" class="form-control" name="emailAdd" <?php echo $email; ?> placeholder="Email Address">
                 </div>
             </div>

             <div class="form-group">
                 <label for="firstName" class="col-md-3 control-label">First Name</label>
                 <div class="col-md-9">
                     <input type="text" class="form-control" name="firstNameAdd" <?php echo $firstName; ?>placeholder="First Name">
                 </div>
             </div>
             <div class="form-group">
                 <label for="lastName" class="col-md-3 control-label">Last Name</label>
                 <div class="col-md-9">
                     <input type="text" class="form-control" name="lastNameAdd" <?php echo $lastName; ?>placeholder="Last Name">
                 </div>
             </div>
             <div class="form-group">
                 <label for="tlf" class="col-md-3 control-label">Telephone</label>
                 <div class="col-md-9">
                     <input id="tlf" type="int" class="form-control" name="tlfAdd" <?php echo $tlf; ?> placeholder="tlf">
                 </div>
             </div>

             <div class="form-group">
                 <label for="password" class="col-md-3 control-label">Password</label>
                 <div class="col-md-9">
                     <input type="password" autocomplete="new-password" class="form-control" name="passwordAdd" placeholder="Password" autocomplete="off">
                 </div>
             </div>


             <div class="form-check">
               <label class="form-check-label">
                 Admin
                 <input style="margin: 0px 0px 0px 5px;" type="checkbox" name="adminAdd" class="form-check-input" value="1" >
               </label>
             </div>

                   <div class="form-group">
                       <!-- Button -->
                       <div class="col-md-offset-3 col-md-9">
                             <input type="submit" onsubmit="return checkTlf()" id="btn-signup" class="btn btn-info" value="Add User"/>
                               </div>
                       </div>
             </form>
            </div>
           </div>
        </div>
