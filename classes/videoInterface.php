<?php



class VideoInterface {
	var $db;
	var $lastVideoGottenById; // using this to reduce db load an php/html coupling.


	/**
	 * The constructor accepts an object of PDO that contains the database connection.
	 * It is also assumed that the session is started before this file get included.
	 * Also note that $_SESSION, $_GET and $_POST needs to be of type superglobal.
	 *
	 * The constructor checks for a $_GET variable of logout, if it exists the user
	 * is logget out.
	 *
	 * If $_POST['email'] is set a logon is attempted. The password should then be
	 * in $_POST['password'].
	 *
	 * if neither $_GET['logout'] nor $_POST['email'] is set then $_SESSION['uid']
	 * is checked. If this is set the user details (name of the user) is retrieved
	 * for the database. If no user with id $_SESSION['uid'] exists the session
	 * variable is cleared.
	 *
	 * @param PDO $db
	 */

	function __construct ($db) {
		$this->db = $db;
	}

  /**
	 * method that get a video by its id.
	 * returns the video information in an array
	 */
	function getVideoById($id) {
		if ($id == $this->lastVideoGottenById['id']) {
			return $this->lastVideoGottenById;
		}
		$sql = 'SELECT id, filePath, title, description, rating, owner FROM videos WHERE id = ?';
		$sth = $this->db->prepare ($sql);
        $sth->execute (array ($id));
    	$row = $sth->fetch();
    	$result = array ("id" => $row['id'], "filePath" => $row['filePath'], "title" => $row['title'], "description" => $row['description'], "rating" => $row['rating'], "owner" => $row['owner']);
    	$this->lastVideoGottenById = $result;
		return $result;
	}

  /**
	 * method that creates a playlist. The parameters is inserted into the database
	 * does not accept duplicated playlist titles
	 */
	function createPlayList($uid, $title, $description) {
		$sql = 'SELECT title FROM playlists WHERE title=?';
		$sth = $this->db->prepare ($sql);
		$sth->execute (array ($title));
		if ($sth->fetch(PDO::FETCH_ASSOC)) {
			echo "you already have a playlist with this name";
		}
		else {
			$sql = "INSERT INTO playlists (owner, title, description) VALUES (?, ?, ?)";
			$sth = $this->db->prepare ($sql);
			$sth->execute (array ($uid, $title, $description));
		}
	}

  /**
	 * method that ads a video to the playlist. Uses the id of playList and video
	 * to add the video to the playlist
	 */
	function addVideoToPlayList($playListId, $videoId) {
		$sql = 'SELECT COUNT(*) FROM playlistentries WHERE playlistid=?';
	    $sth = $this->db->prepare ($sql);
	    $sth->execute (array ($playListId));
	    $row = $sth->fetch();

		$sql = 'INSERT INTO playlistentries (playlistid, videoid, nrinlist) VALUES (?, ?, ?)';
	    $sth = $this->db->prepare ($sql);
	    $sth->execute (array ($playListId, $videoId, $row[0]));
	}

  /*
	 * currently does nothing. Intended to get the specific playlist by using id.
	 */
	function getPlayListById($playListId) {

	}

/*
*
*/
	function addStudentToTheWatchedList($userId, $videoId, $ownerId) {

    $sql = "SELECT * FROM student_list WHERE userId = ? AND videoId = ?";
    $sth = $this->db->prepare($sql);
    $result = $sth->execute(array($userId ,$videoId));

    if($row = $sth->fetch(PDO::FETCH_ASSOC)) {
      echo "user have seen this video";
    } else {
      $sql = "INSERT INTO student_list (videoId, ownerId, userId) VALUES (?,?,?)";
      $sth = $this->db->prepare($sql);
      $result = $sth->execute(array($videoId, $ownerId, $userId));
      if($result) {
        echo "user have now watched the video";
      } else {
        echo "could not add user to db";
      }
    }
	}

	/*
	 *
	 */
	 function getVideoImageData($videoId) {
			// in feature will use from video Id = post[videoId]
			$sql = "SELECT startTime, endTime, url FROM img_data WHERE videoId = ? ORDER BY startTime";
			$sth = $this->db->prepare($sql);
			$sth->execute(array($videoId));	// Get user info from the database

			$videoInfo = array();
			//$videoInfo[0][0] = 0;
			$i = 0;

			while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			   $videoInfo[$i][0] = $row['startTime'];
			   $videoInfo[$i][1] = $row['endTime'];
			   $videoInfo[$i][2] = $row['url'];
			   $i++;
			}

			//print_r($videoInfo);
			return $videoInfo;
			/*if($videoInfo[0][0] != 0) {
			  return $videoInfo;
			}
			else {
			  return(404);
			}*/
	 }

	/*
 	 *
 	 */
	 function deleteVideoImages($imgData) {
			//success message
			$success = "Images deleted";
			//failure message
			$error = "Something went wrong";
			//variable that starts at null then increases by 1 for each time a img is deleted
			$t = 0;

			if(isset($imgData)) {
			  for($i = 0; $i  < sizeof($imgData); $i++) {
					$sql = "DELETE FROM img_data WHERE url = ?";

					$sth = $this->db->prepare($sql);
					$sth->execute (array ($imgData[$i]));
			    if(unlink($imgData[$i])) {
			      $t++;
			    }
			  }
			}

			if ($t == sizeof($imgData)) {
			  print_r($success);
			}
			else {
			  print_r($error);
			}
	 }

  /*
	 * method that gets all the user playlists by using the users id. returns a array
	 * where every entry is another array with playøost information
	 */
	function getPlaylistsByUserId($userId) {
		$sql = 'SELECT id, owner, title, description FROM playlists WHERE owner = ?';
		$sth = $this->db->prepare ($sql);
        $sth->execute (array ($userId));

        $i = 0;
		$playlists = array();
    	while($row = $sth->fetch()) {
        $playlists[$i] = array ("id" => $row['id'], "owner" => $row['owner'], "filePath" => $row['filePath'], "title" => $row['title'], "description" => $row['description']);
			$i++;
		}
		return $playlists;
	}

  /*
	 * method that get all videos belonging to a playlist. Only takes the parameter playlistId to get the videos.
	 * returns an array where every entry is an array with the video information and id of the owner
	 */
	function getVideosByPlaylistId($playlistId) {
		$sql = 'SELECT videos.id, videos.owner, videos.filePath, videos.title, videos.description, videos.rating FROM videos INNER JOIN playlistentries ON videos.id = playlistentries.videoid INNER JOIN playlists ON playlists.id  = playlistentries.playlistid WHERE playlistid = ? ORDER BY playlistentries.nrinlist ASC';
		$sth = $this->db->prepare ($sql);
        $sth->execute (array ($playlistId));

        $i = 0;
		$videos = array();
    	while($row = $sth->fetch()) {
        $videos[$i] = array ("id" => $row['id'], "owner" => $row['owner'], "filePath" => $row['filePath'], "title" => $row['title'], "description" => $row['description'], "rating" => $row['rating']);
			$i++;
		}
		return $videos;
	}

	/*
	*
	*/
	function getStudentWatchedList($videoId) {

	  $sql = "SELECT users.email FROM student_list INNER JOIN users ON users.uId = student_list.userId WHERE videoId = ?";
	  $sth = $this->db->prepare($sql);
		$sth->execute(array($videoId));

	  $studentInfo = array();

		$i = 0;

	  while($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$studentInfo[$i][] = $row['email'];
				$i++;
		}

  	return $studentInfo;
	}

	/**
	 * deletes a video. Uses the id of the video and deletes it '
	 */
	function deleteVideo($id) {
		// deletes both from the videos table and the playlistentries table
		$sql = 'DELETE from videos WHERE id=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($id));

		$sql = 'DELETE from playlistentries WHERE videoid=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($id));
	}

  /**
	 * method for updating video information. Only title and description can be
	 * updated
	 */
	function updateVideoInfo($id, $title, $description) {
		$sql = 'UPDATE videos SET title=?, description=? WHERE id=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($title, $description, $id));
	}

  /**
	 * method that updates the position of the video in the playlist
	 */
	function updateVideoPositionInPlaylist($playlistId, $videoId, $newPosition) {
		$sql = 'UPDATE playlistentries SET nrinlist=? WHERE playlistid=? and videoid=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($newPosition, $playlistId, $videoId));
	}

  /**
	 * method that removes a video from the playlist
	 */
	function removeVideoFromPlaylist($playlistId, $videoId) {
		$sql = 'DELETE from playlistentries WHERE playlistid=? and videoid=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($playlistId, $videoId));
	}


	// updates the title and description of a playlist given id
	function updatePlaylistInfo($id, $title, $description) {
		$sql = 'UPDATE playlists SET title=?, description=? WHERE id=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($title, $description, $id));
	}

	//deletes a playlist given playlist id and deletes all playlist entries accocisated with it
	function deletePlaylist($id) {
		$sql = 'DELETE from playlistentries WHERE playlistid=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($id));

		$sql = 'DELETE from playlists WHERE id=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($id));
	}


 /**
  * method that gets all the videos from a search from the user. This information
	* updated when the user have entered it search and is a very basic search function.
	* checks in the videos table for videos containing specific letters in the specific
	* order entered by the user (based on the title of the video).
	* returns an array of all selected rows using an array containing arrays of video information
  */
	function getVideosBySearch($search, $page, $nrPrPage) {	// this function is severly unoptimised, need redesign
		$sql = 'SELECT id, owner, filePath, title, description FROM videos WHERE title LIKE ?';
		$sth = $this->db->prepare ($sql);

        $sth->execute (array ("%".$search."%"));

        $i = 0;
        $j = 0;
		$videos = array();
    	while($row = $sth->fetch()) {
    		$startIndex = ($page - 1) * $nrPrPage;
    		if ($i >= $startIndex && $i < $startIndex + $nrPrPage) {
		        $videos[$j] = array ("id" => $row['id'], "owner" => $row['owner'], "filePath" => $row['filePath'], "title" => $row['title'], "description" => $row['description']);
		        $j++;
			}
			$i++;
		}
		return array ("results" => $videos, "nrOfResults" => $i);
	}

	function getPlaylistsBySearch($search, $page, $nrPrPage) {	// this function is severly unoptimised, need redesign
		$sql = 'SELECT id, owner, title, description FROM playlists WHERE title LIKE ?';
		$sth = $this->db->prepare ($sql);

        $sth->execute (array ("%".$search."%"));

        $i = 0;
         $j = 0;
		$playlists = array();
    	while($row = $sth->fetch()) {
    		$startIndex = ($page - 1) * $nrPrPage;
    		if ($i >= $startIndex && $i < $startIndex + $nrPrPage) {
		        $playlists[$j] = array ("id" => $row['id'], "owner" => $row['owner'], "title" => $row['title'], "description" => $row['description']);
		        $j++;
			}
			$i++;
		}
		return array ("results" => $playlists, "nrOfResults" => $i);
	}

	function getVideosByUuserId($uid) {
		$sql = 'SELECT id, filePath, title, description FROM videos WHERE owner = ? ORDER BY id DESC';
		$sth = $this->db->prepare ($sql);
        $sth->execute (array ($uid));

        $i = 0;
		$videos = array();
    	while($row = $sth->fetch()) {
        $videos[$i] = array ("id" => $row['id'], "owner" => $row['owner'], "filePath" => $row['filePath'], "title" => $row['title'], "description" => $row['description']);
			$i++;
		}
		return $videos;
	}

  /**
	 * method that gives a video a rating. Only recives the information of the
	 * users rating and the video id to store the value into a rating table in the db
	 */
  function rateVideo($rating, $videoid) {
		$sql = 'SELECT rating FROM ratings WHERE uid = ? AND videoid = ?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($_SESSION['uid'], $videoid));
		if ($res = $sth->fetch()) {
			$sql = 'UPDATE ratings SET rating=? WHERE uid = ? AND videoid = ?';
			$sth = $this->db->prepare($sql);
			$sth->execute (array ($rating, $_SESSION['uid'], $videoid));
	 	} else {
			$sql = 'INSERT INTO ratings VALUES (?,?,?);';
			$sth = $this->db->prepare($sql);
			$sth->execute (array ($videoid, $_SESSION['uid'], $rating));
		}
		$this->setAvgRating($videoid);
	}

  /**
	 * Sets a avg rating for the video. This is done every time a rating a new ratinn
	 * has bin registered. Always selects all previous ratings of the video and finds the new avarage
	 * while this is not very effective it gives some functionality
	 */
	function setAvgRating($videoid) {
		// select all previous ratings for the video
			$sql = 'SELECT rating FROM ratings WHERE videoid = ?';
			$sth = $this->db->prepare($sql);
			$sth->execute (array ($videoid));
			$numberOfResults = 0;
			$totalOfResults = 0;
		  while ($row = $sth->fetch()) {
				$totalOfResults += $row['rating'];
				$numberOfResults++;
			}
			// creates a new average rating for the video then ads it to the db
			$resultAvg = $totalOfResults/$numberOfResults;
			$sql = 'UPDATE videos SET rating=? WHERE id=?';
			$sth = $this->db->prepare($sql);
			$sth->execute (array ($resultAvg, $videoid));
	}

  /**
	 * get the rating of current video
	 */
	function getRating($videoid) {
		// find and returns the rating given by the user
		$sql = 'SELECT rating FROM ratings WHERE videoid=? AND uid=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($videoid, $_SESSION['uid']));
		if($row = $sth->fetch()) {
			return $row['rating'];
		}
		// if user has not rated the video it finds and returns avgRating of video
		else {
			$sql = 'SELECT rating FROM videos WHERE id=?';
			$sth = $this->db->prepare($sql);
			$sth->execute (array ($videoid));
			if($row = $sth->fetch()) {
				return $row['rating'];
			}
			// returns null if there was rating on the video
			else return 0;
		}
	}

  /**
	 * method which returns the subtitles for a video using the videos id
	 */
	function getSubtitlesByVideoId($videoId) {
		$sql = 'SELECT filePath, language, languageCode FROM subtitles WHERE videoId = ?';
		$sth = $this->db->prepare ($sql);
        $sth->execute (array ($videoId));

        $i = 0;
		$subtitles = array();
    	while($row = $sth->fetch()) {
        $subtitles[$i] = array ("filePath" => $row['filePath'], "language" => $row['language'], "languageCode" => $row['languageCode']);
			$i++;
		}
		return $subtitles;
	}


	function updateSubtileFilePath($oldFilePath, $newFilePath){
		$sql = 'UPDATE subtitles SET filePath=? WHERE filePath=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($newFilePath, $oldFilePath));
	}
}

// creates a user object
$videoInterface = new VideoInterface($db);

?>
