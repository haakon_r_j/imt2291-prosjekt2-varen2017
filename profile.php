<?php
  $id = $_POST['id'];
  $ownsProfile = $_POST['ownsProfile'];
  $admin = $_POST['admin'];
?>

<link rel="stylesheet" href="css/global.css">
<link rel="stylesheet" href="css/profile.css">

<style type="text/css" media="screen">
	
#uploadInProgress {
	margin-left: 10px;
	display: none;
}

 .deleted-card {
      opacity: 0.5;
      color: red;
    }


</style>

<div class="container-fluid contentContainer">

	<button type="button" class="btn btn-warning" id="toggleMyVideosEditBtn">Toggle edit</button>
	<button type="button" class="btn btn-danger" id="deleteSelectedVideosBtn">Delete selected</button>


	<div class="container cardsContainer">

	<div class='row'>
		<h2><strong>My Videos</strong></h2>
	</div>

	<div class='row'>
		<p>
	  <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#uploadVideoDiv" aria-expanded="false" aria-controls="uploadVideoDiv">
	    Upload Video
	  </button>
	  <span id="uploadInProgress"><img src="videoLoader.gif">Uploading...</span>

	</div>

	<div class='row'>
	</p>
		<!-- Dropdown for uploading videos-->

		<div class="collapse" id="uploadVideoDiv">
		  <form action="upload.php" method="post" enctype="multipart/form-data" id="newVideoForm">
			  <div class="form-group">
			    <label for="newVideotitleInput">Title</label>
			    <input type="text" class="form-control" id="newVideotitleInput" placeholder="Enter title">
			  </div>
			  <div class="form-group">
			    <label for="newVideoDescriptionInput">Description</label>
			    <input type="text" class="form-control" id="newVideoDescriptionInput" placeholder="Enter description">
			  </div>
			  <div class="form-group">
			    <label for="newVideoFileInput">Select video to upload</label>
			    <input type="file" class="form-control-file" name="newVideoFileInput" id="newVideoFileInput" aria-describedby="fileHelp">
			  </div>
		  	  Subtitles<i class="fa fa-plus" id="addSubtitleDiv" aria-hidden="true"></i>
		  	  <div id=subtitlesUploadDiv>
		  	  	<ul>
		  	  
		  	  	</ul>
		  	  </div>
		  	  <button type="button" id="newVideoSubmitBtn" class="btn btn-primary">Submit</button>

			</form>
		</div>
	</div>

	<!-- Modal for editing video info-->
	<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editVideoModalTitle" aria-hidden="true" id="editVideoModal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="editVideoModalTitle">Edit video details</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      

			  	<div class="form-group">
			    	<label for="editTitleInput">Title</label>
			    	<input type="text" class="form-control" id="editTitleInput">
			  	</div>
				<div class="form-group">
			   	 	<label for="editDescriptionInput">Description</label>
			    	<textarea class="form-control" id="editDescriptionInput" rows="3"></textarea>
			  	</div>


	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="videosEditModalSaveChangesBtn">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>


	<!-- This will contain the videos on the profile-->
	<div id="profileVideosContainer">
	

	</div>


</div>



	<div class="container-fluid cardsContainer">

		<div class='row'>
			<h2><strong>My Playlists</strong></h2>
		</div>

		<div class='row'>
			<p>
		  <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#newPlaylistDiv" aria-expanded="false" aria-controls="newPlaylistDiv">
		    New Playlist
		  </button>
		  </p>
		</div>

		<div class='row'>
			<!-- dropdown for creating new playlist-->
			<div class="collapse" id="newPlaylistDiv">
			  <form>
				  <div class="form-group">
				    <label for="newPlaylistTitleInput">Title</label>
				    <input type="text" class="form-control" id="newPlaylistTitleInput" placeholder="Enter title">
				  </div>
				  <div class="form-group">
				    <label for="newPlaylistDescriptionInput">Description</label>
				    <input type="text" class="form-control" id="newPlaylistDescriptionInput" placeholder="Enter description">
				  </div>
			  	<button type="button" class="btn btn-primary">Submit</button>
			  </form>
			</div>
		</div>

		
 	<button type="button" class="btn btn-warning" id="toggleEditPlaylists">Toggle edit</button>
		<div class='row'>

			<!-- This will contain the playlists on the profile-->
			<div id='playlistsListDiv'>
				<ul class='list-group'>		 

				
				</ul>
			</div>


			<div id="sortPlaylistsDiv">
				<ol class='sortable'>

				</ol>
			</div>
			
		</div>

		

	</div>

	<!-- Modal for editing playlist info-->
	<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editPlaylistModalTitle" aria-hidden="true" id="editPlaylistModal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="editPlaylistModalTitle">Edit video details</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      

			  	<div class="form-group">
			    	<label for="editPlaylistTitleInput">Title</label>
			    	<input type="text" class="form-control" id="editPlaylistTitleInput">
			  	</div>
				<div class="form-group">
			   	 	<label for="editPlaylistDescriptionInput">Description</label>
			    	<textarea class="form-control" id="editPlaylistDescriptionInput" rows="3"></textarea>
			  	</div>


	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="playlistsEditModalSaveChangesBtn">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>


</div>



<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

	<script src="js/jquery.sortable.js"></script>


<script type="text/javascript" charset="utf-8" async defer>

var profileId = <?php echo $id; ?>;
var videosArray = [];
var playlistsArray = [];

getVideosFromServer(profileId);
getPlaylistsFromServer(profileId);

// clicking on a video load sthe watch page into the main element of the page.
$("#profileVideosContainer").on('click', 'a', function(event) {
	event.preventDefault();
	watchVideo($(this).attr('data-id'));
	// $("#main_element").load('watch.php', { "id" : $(this).attr('data-id'), "isPlaylist": '0' });
});


// clicking on a playlist load sthe watch page into the main element of the page and specifies it is a playlist.
$("#playlistsListDiv").on('click', 'a', function(event) {
	event.preventDefault();
	console.log($(this).attr('data-id'));
	watchPlaylist($(this).attr('data-id'));
	// $("#main_element").load('watch.php', { "plId" : $(this).attr('data-id'), "isPlaylist": '1' });
});

 
$('#newVideoSubmitBtn').click(function(event) { 	
	var title1 = $('#newVideotitleInput').val();								//TODO validation
	var description1 = $('#newVideoDescriptionInput').val();

	var formData = new FormData();
	formData.append('fileToUpload', $('#newVideoFileInput').prop('files')[0]);
	
	for (i = 0; i < $('#subtitlesUploadDiv input').length; i++) {
		formData.append('subtitleFiles[' + i + ']', $('#subtitlesUploadDiv input')[i].files[0]);
	}

	for (i = 0; i < $('#subtitlesUploadDiv select').length; i++) {
		formData.append('languages[' + i + ']', $('#subtitlesUploadDiv select').eq(i).val());
	}


	formData.append('uid', profileId);
	formData.append('title', $('#newVideotitleInput').val());
	formData.append('description', $('#newVideoDescriptionInput').val());

	$("#uploadInProgress").show();
	

	$.ajax({
		context: this,
		url: 'upload.php',
		data : formData,
       	processData: false,  // tell jQuery not to process the data
       	contentType: false,  // tell jQuery not to set contentType
       	type: 'POST',
	})
	.done(function(data) {
		console.log("successnewvideo");
		getVideosFromServer(profileId);
		console.log(data);
	})
	.fail(function() {
		console.log("errornewvideo");
	})
	.always(function(data) {
		console.log("complete");
		$("#uploadInProgress").hide();
	});
});

// when clicking the submit new playlist button the new playlist is added to the databse and displayed.
$('#newPlaylistDiv button').click(function(event) {
	var title1 = $('#newPlaylistTitleInput').val();								//TODO validation
	var description1 = $('#newPlaylistDescriptionInput').val();

	$.ajax({
		context: this,
		url: 'ajax.php',
		type: 'POST',
		data: {action: "NEW_PLAYLIST", uid: profileId, title: title1, description: description1},
	})
	.done(function(data) {
		console.log("success11");
		getPlaylistsFromServer(profileId);
	})
	.fail(function() {
		console.log("error11");
	})
	.always(function(data) {
		console.log("complete");
	});
});


// this function displays the list of videos stored in the videos array
// when videos are loaded or a video is added or removed, simply call this to update the view.
function displayVideos() {
	var html = "<div class='row card-row'>" +
					"<div class='card-deck' id='videosDeck'>";
	var i;					
	for (i = 0; i < videosArray.length; i++) {
		html += "";
		if (!(i % 4) && i != 0) {
			html += "</div>" +
				"</div>" +
			"<div class='row card-row'>" +
				"<div class='card-deck'>";
		}

		html += 
		"<div class='card my-videoes-card'>" +
			"<div>" +
				"<i class='fa fa-pencil fa-2x myVideosEditBtn' aria-hidden='true'></i>" +
				"<i class='fa fa-times fa-2x myVideoesDeleteBtn' aria-hidden='true'></i>" +
			"</div>" +
			"<a href='#' data-id='" + videosArray[i]['id'] + "'>" +
				"<img class='card-img-top' src='https://fbnewsroomus.files.wordpress.com/2015/04/messenger-video-call-carousel-icon.png?w=960' alt='Card image cap'>" +
				"<div class='card-block'>" +
	      			"<h5 class='card-title'>" + videosArray[i]['title'] + "</h4>" +
	    		"</div>" +
	    	"</a>" +
	    "</div>";

	}

	// we fill the ramaing of the row with hidden cards so that the bottom videos don't get upscaled
	if (i % 4) {
		for (j = 4 - i % 4; j > 0; j--) {
			html +=	
			"<div class='card hidden-card'>" +
		    	"<img class='card-img-top' src='http://www.imgworlds.com/wp-content/uploads/2015/11/img-blvd.jpg' alt='Card image cap'>" +
		    	"<div class='card-block'>" +
		      		"<h5 class='card-title'>Video title</h4>" +
		      		"<p class='card-text'><small class='text-muted'>Added 3 mins ago</small></p>" +
		    	"</div>" +
		  	"</div>";
		}
	}


	html += "</div>" +
		"</div>";

	//$("#profileVideosContainer").html("");
	$("#profileVideosContainer").html(html);
}

function displayPlaylists() {
	var html = "";
	for (i = 0; i < playlistsArray.length; i++) {
		html += 
		"<li class='list-group-item' data-id='" + playlistsArray[i]['id'] + "' data-title='" + playlistsArray[i]['title'] + "'data-description='" + playlistsArray[i]['description'] + "'>" +
			"<div>" + 
				"<a href='#' data-id='" + playlistsArray[i]['id'] + "'>" +
					"<i class='fa fa-play-circle-o fa-2x' aria-hidden='true'></i>" +
				"</a>" +
				"<h5>" + playlistsArray[i]['title'] + "</h5>" +
			"</div>" +	
			"<i class='fa fa-pencil fa-2x hidden-unless-edit-playlists' aria-hidden='true'></i>" +
			"<i class='fa fa-times fa-2x hidden-unless-edit-playlists' aria-hidden='true'></i>" +
		"</li>";

	}

	$("#playlistsListDiv ul").html(html);
}

// retrived whe videos for the profile form the server, then displays them	
function getVideosFromServer(uid) {
	$.ajax({
		context: this,
		url: 'ajax.php',
		type: 'POST',
		data: {action: "GET_VIDEOS_BY_USER_ID", uid: uid},
		dataType: "json",
	})
	.done(function(data) {
		console.log("success");
		videosArray = data;
		displayVideos();
	})
	.fail(function() {
		console.log("error");
	})
	.always(function(data) {
		console.log("complete");
	});
	
}


function getPlaylistsFromServer(uid) {
	$.ajax({
		context: this,
		url: 'ajax.php',
		type: 'POST',
		data: {action: "GET_PLAYLISTS_BY_USER_ID", uid: uid},
		dataType: "json",
	})
	.done(function(data) {
		console.log("success22");
		playlistsArray = data;
		displayPlaylists();
	})
	.fail(function() {
		console.log("error22");
	})
	.always(function(data) {
		console.log("complete");
	});
} 



// When the cross to delete a video is clicked we mark the video for removal by adding the "deleted-card" class.
	// We can then later remove all the marked videos when the big red delete button is clicked.
	$('#profileVideosContainer').on('click', '.myVideoesDeleteBtn', function(event) {
		console.log('test2'); 
		$(this).closest(".my-videoes-card").toggleClass('deleted-card'); 
	});


// When the edit icon for a video is clicked we show the modal and load inn the title and description.
	// Any changes done will be saved in the database when the save changes button is clicked and we update the card title.
$('#profileVideosContainer').on('click', '.myVideosEditBtn', function(event) {
		$('#editVideoModal').modal('show');

		cardTitleInModal = $(this).closest('.my-videoes-card').find('.card-title');
		$('#editTitleInput').val(cardTitleInModal.text());			// sets the title text in the modal to the current title.
		$('#editDescriptionInput').val("");

		var videoId = $(this).closest('.my-videoes-card').find('a').attr('data-id');

		videoInModalId = videoId;

		$.ajax({					// ajax call that returns the description of the video as we dont have it stored in here.					
				context: this,					
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'GET_VIDEO_DESCRIPTION', id: "" + videoId},
			})
			.done(function(data) {	
				$('#editDescriptionInput').val(data);  	// sets the description text in the modal to the current description.
				
			})
			.fail(function() {
				alert("error");
			});
	});


var currentRequest = null; 
// Set the clicked playlist to active and the other to not active.
$('#playlistsListDiv').on('click', 'li', function(event) {
		if ($(this).hasClass('active')) {
			return;
		}	
		$("#playlistsListDiv li").removeClass('active');
		$(this).addClass('active');
		$("#sortPlaylistsDiv ol").empty();
		var videoId = $('#playlistsListDiv .active').attr('data-id');

		
		// we want to abort the previous ajax call if a new one is made, so that e dont accidently load videos from multiple playlists together.
		currentRequest = jQuery.ajax({								
			context: this,					
			url: 'ajax.php',
			type: 'POST',
			data: {action: 'GET_VIDEOS_IN_PLAYLIST', id: "" + videoId},
			dataType: "json",
			beforeSend : function()    {           
		        if(currentRequest != null) {
		            currentRequest.abort();
		        }
	   		},
		})

		// when we succesfully got returned the videos in the list we need to display them, make them sortable and add click listeners for deleting them.
		.done(function(data) {	
			for(var i in data) {
       		  $("#sortPlaylistsDiv ol").append("<li data-id='" + data[i]['id'] + "'><strong>" + data[i]['title'] + "</strong><i class='fa fa-times fa-2x' aria-hidden='true'></i></li>");
    		}
    		$('.sortable').sortable();

    		// whwen we click the X to remove a video form the playlist, we do a ajax call that will remove the entry from the databse.
			$("#sortPlaylistsDiv .fa-times").click(function(event) {
				$.ajax({								
					context: this,					
					url: 'ajax.php',
					type: 'POST',
					data: {action: 'REMOVE_VIDEO_FROM_PLAYLIST', playlistId: "" + videoId, videoId: "" +  $(this).closest('li').attr('data-id')},
				})
				.done(function(data) {	
					// the entry no longer exist in the playlist and we remove it visually.
					$(this).closest('li').toggle();
					
				})
				.fail(function() {
					alert("error");
				});

			});
			
		})
		.fail(function() {
			
		});
	});







		var videoInModalId; // nasty little variable so we can easily get the id of the video we are editing inside the modal.
	var cardTitleInModal; // same as above, used to set the title of the card without updating the page.


	// When we click the edit button we simply toggle the controls
	$("#toggleMyVideosEditBtn").click(function(event) {
		$(".myVideoesDeleteBtn").toggle();
		$(".myVideosEditBtn").toggle();
		$("#deleteSelectedVideosBtn").toggle();
		$(".my-videoes-card").removeClass('deleted-card');
	});




	// Deletes all videos marked for removal and have them removed from the database using a ajax call.
	$("#deleteSelectedVideosBtn").click(function(event) {
		var idsToDelete = $('.deleted-card').map(function(){			// gets the ids of every video marked for removal
   			return $(this).children("a").attr('data-id');
		}).get();

		$.ajax({										// we pass in the array of ids to delete and they will be removed from the server and database.
				context: this,
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'DELETE_VIDEOS', ids: idsToDelete},
			})
			.done(function(data) {
				getVideosFromServer(profileId);  // Displays the new videos layout.
				
			})
			.fail(function() {
				alert("error");
			})
		
	});


	// when the save changes button is clicked we take the text from the input fields, and make an ajax call passing in this info
	// along with the id. As a result they will be updated in the database.
	$("#videosEditModalSaveChangesBtn").click(function(event) {
		var newTitle = $('#editTitleInput').val();
		var newDescription = $('#editDescriptionInput').val();

		$.ajax({								
				context: this,					
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'UPDATE_VIDEO_INFO', id: "" + videoInModalId, title: "" + newTitle, description: "" + newDescription},
			})
			.done(function(data) {	
				$('#editVideoModal').modal('hide');
				$(cardTitleInModal).text(newTitle);
			
				
			})
			.fail(function() {
				alert("error");
			});
	});

	var currentRequest = null; 

	// Set the clicked playlist to active and the other to not active.
	$('#playlistsListDiv').on('click', 'li', function(event) {
		if ($(this).hasClass('active')) {
			return;
		}	
		$("#playlistsListDiv li").removeClass('active');
		$(this).addClass('active');
		$("#sortPlaylistsDiv ol").empty();
		var videoId = $('#playlistsListDiv .active').attr('data-id');

		
		// we want to abort the previous ajax call if a new one is made, so that e dont accidently load videos from multiple playlists together.
		currentRequest = jQuery.ajax({								
			context: this,					
			url: 'ajax.php',
			type: 'POST',
			data: {action: 'GET_VIDEOS_IN_PLAYLIST', id: "" + videoId},
			dataType: "json",
			beforeSend : function()    {           
		        if(currentRequest != null) {
		            currentRequest.abort();
		        }
	   		},
		})

		// when we succesfully got returned the videos in the list we need to display them, make them sortable and add click listeners for deleting them.
		.done(function(data) {	
			for(var i in data) {
       		  $("#sortPlaylistsDiv ol").append("<li data-id='" + data[i]['id'] + "'><strong>" + data[i]['title'] + "</strong><i class='fa fa-times fa-2x' aria-hidden='true'></i></li>");
    		}
    		$('.sortable').sortable();

    		// whwen we click the X to remove a video form the playlist, we do a ajax call that will remove the entry from the databse.
			$("#sortPlaylistsDiv .fa-times").click(function(event) {
				$.ajax({								
					context: this,					
					url: 'ajax.php',
					type: 'POST',
					data: {action: 'REMOVE_VIDEO_FROM_PLAYLIST', playlistId: "" + videoId, videoId: "" +  $(this).closest('li').attr('data-id')},
				})
				.done(function(data) {	
					// the entry no longer exist in the playlist and we remove it visually.
					$(this).closest('li').toggle();
					
				})
				.fail(function() {
					alert("error");
				});

			});
			
		})
		.fail(function() {
			
		});
	});




	// when the order of the videos ina  list changes, we store that in the database.
	$('.sortable').sortable().bind('sortupdate', function() {

		var idsAndPositions = $("#sortPlaylistsDiv li").map(function(){		
			var id = $(this).attr('data-id');	
			var position = $(this).parent().children().index(this);

			var obj = new Object();
			obj.id = id;
		   	obj.position = position;
		   	var jsonString = JSON.stringify(obj);
			return jsonString;
		}).get();

   		var playlistId = $('#playlistsListDiv .active').attr('data-id');
   		


    	$.ajax({								
				context: this,					
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'UPDATE_PLAYLIST_VIDEO_ORDER', playlistId: "" + playlistId, idsAndPositions: idsAndPositions},
			})
			.done(function(data) {	
				
				
			})
			.fail(function() {
				alert("error");
		});
	
	});

	// when the + sign in the upload video from is clicked we add a subtitle file field.
	$("#addSubtitleDiv").click(function(event) {
		$("#subtitlesUploadDiv ul").append("<li><input type='file' name='subtitleFiles[]' class='form-control-file' aria-describedby='fileHelp'><select class='form-control' name='languages[]'> <option>English</option><option>Norwegian</option><option>Spanish</option><option>French</option><option>German</option></select></li>");
	});

	// deletes playlist when the X is clicked
	$("#playlistsListDiv").on('click', '.fa-times', function(event) {
		console.log($(this).closest('li').attr('data-id'));
		
    	$.ajax({								
				context: this,					
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'DELETE_PLAYLIST', id: "" + $(this).closest('li').attr('data-id')},
			})
			.done(function(data) {	
				$(this).closest('li').toggle();	
			})
			.fail(function() {
				alert("error");
		});
	
	});


	// some uggly variables wwe use to easily acees the info of the playlist currently in the modal
	var liInPlaylistModal;
	var playlistInModaltitle = "";
	var playlistInModalDescription = "";
	var playlistInModalId;

	// when we click on the edit pencil fo a playlist wwe display the modal and loads in the info.
	$("#playlistsListDiv").on('click', '.fa-pencil', function(event) {
		console.log("ddsdsds");
		
		$('#editPlaylistModal').modal('show');

		liInPlaylistModal = $(this).closest('li');
		playlistInModaltitle = $(this).closest('li').attr('data-title');
		playlistInModalDescription = $(this).closest('li').attr('data-description');
		playlistInModalId = $(this).closest('li').attr('data-id');
		$('#editPlaylistTitleInput').val(playlistInModaltitle);			
		$('#editPlaylistDescriptionInput').val(playlistInModalDescription);

		
	});

	// wwhen the save chnages button is clicked we save the changed to the database.
	$("#playlistsEditModalSaveChangesBtn").click(function(event) {
		var newTitle = $('#editPlaylistTitleInput').val();
		var newDescription = $('#editPlaylistDescriptionInput').val();
		

		$.ajax({								
				context: this,					
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'UPDATE_PLAYLIST_INFO', id: "" + playlistInModalId, title: "" + newTitle, description: "" + newDescription},
			})
			.done(function(data) {	
				$('#editPlaylistModal').modal('hide');
				$(liInPlaylistModal).find('h5').html(newTitle);
				$(liInPlaylistModal).attr('data-title', newTitle);
				$(liInPlaylistModal).attr('data-description', newDescription);
				
			})
			.fail(function() {
				alert("error");
			});
	});

	$("#toggleEditPlaylists").click(function(event) {
		$(".hidden-unless-edit-playlists").toggle();
	});


</script>


