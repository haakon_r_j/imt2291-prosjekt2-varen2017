<?php
  session_start();
  require_once 'include/db.php';    // Connect to the database
  require_once 'classes/user.php';
  require_once 'classes/videoInterface.php';

  //for quickly adding users

  /*$user->addUser('kdfkdmfdf@hotmail.no', 'kultpw', 'Abelone', 'Bertestad', 91565911, 0);
  $user->addUser('kjkjkjj@hotmail.no', 'fdfdf', 'Mari', 'Anulfsen', 94453452, 0);
  $user->addUser('bnnnbbn@hotmail.no', 'jjhjhj', 'Thomas', 'Arnolfsen', 91457942, 0);
  $user->addUser('kjhgd@hotmail.no', 'lklkl', 'Loke', 'Olsen', 99437222, 1);
  $user->addUser('dddddd@hotmail.no', '3242f3', 'Bjarne', 'Landerud', 98221532, 0);
  $user->addUser('hghgjhj@hotmail.no', 'kultpw', 'Sverre', 'Hagen', 46374855, 0);
  $user->addUser('kags@hotmail.no', 'kultpw', 'Truls', 'Bertulfsen', 92738920, 0);
  $user->addUser('pouuuu@hotmail.no', 'kultpw', 'Maria', 'Bjarnesen', 49982934, 0);
  $user->addUser('kattepus@hotmail.no', 'kultpw', 'Yngve', 'Hattestad', 92837928, 1);
  $user->addUser('kattepus@hotmail.no', 'kultpw', 'Nikoline', 'Tullestad', 98273846, 0);*/
?>


<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="css/global.css">

  <title>index</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="css/header.css">

    <link rel="stylesheet" href="css/index.css">

      <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script
      src="https://code.jquery.com/jquery-3.1.1.js"
      integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
      crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous">
    </script>


    <script type="text/javascript" src="js/header.js">
    </script>

</head>

  <body>
  <div id="header-div">

  </div>

   </div>
    <div id="main_element">


    </div>



    <script type="text/javascript" charset="utf-8" async defer>

      loadHeader();

      function loadHeader() {

        $('#header-div').load('header.php');

      }

      function watchVideo(id) {
        $("#side-navbar .nav-item, #topNavbar .nav-item").removeClass('active');
        $("#main_element").load('watch.php', { "id" : id, "isPlaylist": '0' });
      }

      function watchPlaylist(id) {
        $("#side-navbar .nav-item, #topNavbar .nav-item").removeClass('active');
        $("#main_element").load('watch.php', { "plId" : id, "isPlaylist": '1' });
      }


        $("#main_element").load("frontpage.php");

        $("#header-div").on('click', '#topNavbar .nav-item', function(event) {
            if ($(this).hasClass('active')) {
              return;
            }
            $("#topNavbar .nav-item, #side-navbar .nav-item").removeClass('active');
            $(this).addClass('active');

            var mainElement = $("#main_element");
            mainElement.empty();
            mainElement.load($(this).attr('data-file'));
            //alert($(this).attr('data-file'));
        });


        $("#header-div").on('click', '#side-navbar .nav-item', function(event) {
            if ($(this).hasClass('active')) {
              return;
            }
            $("#side-navbar .nav-item, #topNavbar .nav-item").removeClass('active');
            $(this).addClass('active');

            var sessionUid = <?php if (isset($_SESSION['uid'])) {echo $_SESSION['uid'];} else {echo -1;} ?>;
            if (sessionUid != -1) {
              var mainElement = $("#main_element");
              mainElement.empty();
              mainElement.load($(this).attr('data-file'), { "id": sessionUid, "ownsProfile" : "1", "admin" : "0"});
              //alert($(this).attr('data-file'));
            }
        });

    </script>

</body>


</html>
