


    $(".playlist-choice").click(function(event) {

        $.ajax({
                context: this,
                url: 'watch.php',
                type: 'POST',
                data: {action: 'ADD_VIDEO_TO_PLAYLIST', playlistId: "" + $(this).attr('data-playlistId'), videoId: "" + $(this).attr('data-videoId')},
            })
            .done(function() {

            })
            .fail(function() {
                alert("error");
            })
            .always(function() {

            });

    });

    // playlist js start

  // the playlist is 5 videos in length visually, if we have less videos than that,
  // we wanto to shrink the size of the playlist and diable the arrows as there is nothign to navigate to.
  var nrOfVideosInplaylist = $("#playlistVideosContainer li").length;
  if (nrOfVideosInplaylist < 5) {
    var newContainerwidth = (nrOfVideosInplaylist % 5) * 162 + 2;
    $("#playlistVideosContainer").css('width', "" + newContainerwidth + "px");
    $("#playlistContainer .fa-arrow-right").addClass('faded-arrow');
  }

  // we only want to show 5 videos in the list at a time, so to start we hide every video except the 5 first.
  else if (nrOfVideosInplaylist > 5) {
    $("#playlistVideosContainer li:gt(4)").addClass('hidden-video');
  }

  else {
    $("#playlistContainer .fa-arrow-right").addClass('faded-arrow');
  }

  // if there is videos in the list, we set the title, src and description etc.
  if (nrOfVideosInplaylist > 0) {
      var firstVideo = $("#playlistVideosContainer li").first();
      loadVideofromPlaylist(firstVideo);
    }

  // the left arrow button will always be diabled at first.
  $("#playlistContainer .fa-arrow-left").addClass('faded-arrow');

  // when a video is clicked it is selected by adding the activeVideoInPlayList. the presvious selected video is
  // had this class removed. We set the source in the video player to play this video. we also set title, description etc.
  $("#playlistVideosContainer li").click(function(event) {
    loadVideofromPlaylist($(this))
  });

  //clicking the right arrow we have to push every video 1 step to the left. we do this by hiding the first visible element, and showing
  //the first hidden element that comes after it.
  $("i.fa-arrow-right").click(function(event) {
    var nextVideo = $("#playlistVideosContainer li:not(.hidden-video)").last().next();
    if (nextVideo.hasClass('hidden-video')) {
      $("#playlistVideosContainer li:not(.hidden-video)").first().addClass('hidden-video');
      nextVideo.removeClass('hidden-video');
      // fades the arrow if there is no more videos to the right.
      if(nextVideo.is(':last-child')) {
        $("#playlistContainer .fa-arrow-right").addClass('faded-arrow');
      }
    }
    if (nrOfVideosInplaylist > 5) {
      $("#playlistContainer .fa-arrow-left").removeClass('faded-arrow');
    }
  });


  // same here but for left button
  $("i.fa-arrow-left").click(function(event) {
    var nextVideo = $("#playlistVideosContainer li:not(.hidden-video)").first().prev();
    if (nextVideo.hasClass('hidden-video')) {
      $("#playlistVideosContainer li:not(.hidden-video)").last().addClass('hidden-video');
      nextVideo.removeClass('hidden-video');

      if(nextVideo.is(':first-child')) {
        $("#playlistContainer .fa-arrow-left").addClass('faded-arrow');
      }
    }
    if (nrOfVideosInplaylist > 5) {
      $("#playlistContainer .fa-arrow-right").removeClass('faded-arrow');
    }
  });


  // when a video finishes we want to play the next in the list if there is one.
  $("#video").bind("ended", function() {
    var nextVideo = $(".activeVideoInPlayList").next('li');
    if (nextVideo.attr('data-id') != null) {
       loadVideofromPlaylist(nextVideo); // if its not the last movie, we load the next.
    }

  });


  // This function takes in a li for a video in the playlist. is sets this video to be the active one
  // and loads the title, description and src etc for the film. we then play the new video
  function loadVideofromPlaylist(videoAsLi) {
    $('#playlistVideosContainer .activeVideoInPlayList').removeClass('activeVideoInPlayList');
    videoAsLi.addClass('activeVideoInPlayList');
    var filePath = videoAsLi.attr('data-filePath');
    $("#video").attr('src', filePath);
    $("h2").html(videoAsLi.attr('data-title'));
		$("#video").attr('data-id', videoAsLi.attr('data-id'))
    $("#descriptionDiv").html(videoAsLi.attr('data-description')); // this one is used to easily add a video to a new playlist

		$("#ratingContainer .fa-star").removeClass('activeStar');
		var videoRating = Math.round(parseInt(videoAsLi.attr('data-rating')));
		$("#ratingContainer .fa-star:lt(" + videoRating + ")").addClass('activeStar');

    loadSubtitleTracksForVideo(videoAsLi.attr('data-id'));

    $("#video")[0].play();


  }







//SUBS START


   $( document ).ready(function() {
  var video = document.getElementById('video');  // Finding the video
  video.play();                                  // Starting the video
  addListeners(video);                       // adding listeners for video events
  setTimeout(function(){
    loadTrack($("video").attr("data-lang"));                           // loading the active text track
  }, 100);

                                             // Changing the text track when a link in
                                             // the dropdown is clicked
  $("#textDiv div#languageSelect > ul > li.available").on('click', 'a' ,changeTrack);
                                             // changing the video time and playing
  $("#textDiv").on('click','p',function(){   // video when a line of thext is clicked
    video_settime($(this).data("start"));
    video.play();
  });


  addLanguageSelectClickListeners();


 $("#speedSelect li").click(function(event) {
    if ($(this).hasClass('active')) {
      return;
    }
    $("#speedSelect li").removeClass('active');
    $(this).addClass('active');
    document.querySelector("#video").playbackRate = $(this).html();
  });



});
// Updating the language
// that is set in the dropdown
// and is giving the new language
// the class active
function changeTrack(){
  event.preventDefault();

  var lang = $('#video').attr("data-lang");

  $("#textDiv div#languageSelect > ul > li.active").each(function(){
    $(this).removeClass("active-track")
  })
  $("div#languageSelect button#languageButton span.lang-sm").attr("data-lang", lang);

  $("#textDiv div#languageSelect > ul > li."+lang).addClass("active");

  loadCurrent();
}


function loadSubtitleTracksForVideo (vidId) {
  $.ajax({
          context: this,
          url: 'ajax.php',
          dataType: "json",
          type: 'POST',
          data: {action: 'GET_TRACK_HTML', videoId: "" + vidId},
      })
      .done(function(data) {

          $("#languageSelect ul").html("");
          $("video").html("");


          $("#video").html(data['trackHtml']);
          $("#languageSelect ul").html(data['languageSelectHtml']);

          $("video").attr("data-lang", "");
          $("video").attr("data-lang", $("#languageSelect li.active").attr("data-lang"));


          $("#textDiv").html("");
          addLanguageSelectClickListeners();


          loadTrack($("video").attr("data-lang"));

          setTimeout(triggerClick, 200);



      })
      .fail(function() {
          alert("error");
      });
}

function triggerClick() {
  console.log("aa");
  $("#languageSelect li.active").trigger("click");
}


// Adding listeners for when a video is changing time
// and when the vieo is finished
function addListeners(video) {
 video.addEventListener('timeupdate', update, false);
 video.addEventListener('ended', nextvideo, false);
}


function addLanguageSelectClickListeners() {
  $("#languageSelect li").click(function(event) {

    $("#languageSelect li").removeClass('active');
    $(this).addClass('active');
    $("video").attr("data-lang", $(this).attr("data-lang"));
    loadTrack($("video").attr("data-lang"));

  });
}

// Looking for a link in the playlist div with the class next.
// If it is found user will be sent there
function nextvideo(event){
  if($('#playlist a.next').attr('href')){
    window.location.href=$('#playlist a.next').attr('href');
  }
}

// when the video time is changing it will chack if there is a text
// line that should be marked as the current one giving the
// row class hightligth if it is at the right time removing the higlight
// class if it is no longer the correct time
function update(event){
  $('#textDiv').find('p').each(function(index) {
    var row = $(this);
    var start = row.data('start');
    var end =   row.data('end');
    if(parseFloat(start) <= parseFloat(event.target.currentTime) && parseFloat(end) >= parseFloat(event.target.currentTime) ){
      row.addClass('highlight');
    }else{
      row.removeClass('highlight');
    }
  });


  // here we scroll the subtitle text depending on the position relative to the top
  //of the viible textarea, so the highlighted text is always just slightly above the center

  if (!$("#autoplayBtn").hasClass('autoplayBtnOff')) {
    var currentText = $("#textDiv p.highlight");

    if (currentText.length != 0) {
      var curPos = $("#textDiv p.highlight").position().top;

      if (curPos < 240) {
        var currentScroll = $("#textDiv").scrollTop();
        $("#textDiv").animate({ scrollTop: currentScroll + curPos - 240});
      }

      if (curPos > 240) {
        var currentScroll = $("#textDiv").scrollTop();
        console.log(currentScroll);
        $("#textDiv").animate({ scrollTop: currentScroll + curPos - 240});
       }

       else if (curPos < 0) {

       }
     }

  }

}

// decides wwteher autoscroll is on or the user can freely scroll
$("#autoplayBtn").click(function(event) {
    $(this).toggleClass('autoplayBtnOff');
});




// setting the time of the video to a new time
function video_settime(newtime){
  var video = document.getElementsByTagName("video")[0];
  video.currentTime = newtime;
}

// reading the ques from the track and are displaying the textin the textDiv
function readContent(track) {
  $("#textDiv").html("");
  var cues = track.cues;
  for(var i=0; i < cues.length; i++) {
    var cue = cues[i];

    var id    = cue.id ;
    var start = cue.startTime;
    var end   = cue.endTime;
    var text  = cue.text ;
    var rowData = "<p data-start='" + start + "' data-end='" + end + "'>" + text + "</p>"
    $("#textDiv").append(rowData);
  }
}

// getting a video, and loading the video with the language that was
// send as a parameter
function loadTrack(lang) {
  console.log(lang);
  var video = document.getElementById('video');
  for (var i = 0; i < video.textTracks.length; i++) {
    if (video.textTracks[i].language == lang) {
      video.textTracks[i].mode = 'showing';
      readContent(video.textTracks[i]);
    }else {
      video.textTracks[i].mode = 'hidden';
    }
  }
}


$("#ratingContainer .fa-star").click(function(event) {
	$("#ratingContainer .fa-star").removeClass('activeStar');
	$(this).addClass('activeStar');
	var curSelectedStar = $(this).attr('value');
	var videoId = $("#video").attr('data-id');
	$("#ratingContainer .fa-star:lt(" + curSelectedStar + ")").addClass('activeStar');
	$.ajax({
					context: this,
					url: 'ajax.php',
					type: 'POST',
					data: {action: 'RATE_VIDEO', rating: "" + curSelectedStar, videoId: "" + videoId},
			})
			.done(function(data) {

			})
			.fail(function() {
					alert("error");
			})
			.always(function() {

			});
});






//js haakon

var videoPlayer = document.getElementById("video");

  var duration = 0;
  var currentTime = 0;

  var videoDivWindow = $("#video");
  var videoDivWidth = videoDivWindow.width();
  var videoDivHeight = videoDivWindow.height();

  var windowWidth = $(window).width();
  var windowHeight = $(window).height();

  var videoImages = new Array();
  var tmpCurrentImg = 0;

  var imageIds = Array();
  var imageDivIds = Array();
  var imageCheckboxIds = Array();
  var imageCheckboxDivIds = Array();

//A call that will get all the image data for the video
console.log('fail');

getImageData();

function getImageData() {
  var videoId = $('#video').attr('data-id');
  console.log('sadsd');
  $.ajax({
    context: this,
    type: 'POST',
    url: 'ajax.php',
    data: {action: 'GET-VIDEO-IMAGE-DATA' , videoId: "" + videoId},
    dataType: 'json',
    success: function(response) {

            console.log('success');
              console.log(response);
            if(response == 404) {
              console.log('fail404');
            }
            else {
              //console.log(response);
              videoImages = response;

              //append to the images-container-div elements that will show the images information and other relevant functionality
              for(var i = 0; i < videoImages.length; i++) {
                imageIds[i] = "video-img-div-" + i;
                imageDivIds[i] = "video-img-" + i;
                imageCheckboxIds[i] = "video-image-checkbox-" + i;
                imageCheckboxDivIds[i] = "video-image-checkbox-div-" + i;
                //sets up a div for each image
                $('#images-container-div').append("<div id='" + imageDivIds[i] + "' class='video-image-container'></div>");
                //sets up a div for each checkbox that wil be used to select the image to delete it
                $('#' + imageDivIds[i]).append("<div id='" + imageCheckboxDivIds[i] + "' style='height: 15px; width: auto;'</div>");
                //sets up a checkbox for each image (starts hidden until the edit toggle is on)
                $('#' + imageCheckboxDivIds[i]).append("<input type='checkbox' class='checkbox-image' id='" + imageCheckboxIds[i] + "' name='directory' value='" + videoImages[i][2] + "' style='display: none;'>");
                //sets up each image
                $('#' + imageDivIds[i]).append("<img id='" + imageIds[i] + "' class='video-image' src='" + videoImages[i][2] + "' width='200px' height='120px' style='margin 0px 5px 10px 5px'>");
                // sets up the start and end time for each image
                $('#' + imageDivIds[i]).append("<p>" + videoImages[i][0] + "-" + videoImages[i][1] +"</p>");
              }

              //button that will delete the images
              $('#images-container-div').append("<button id='remove-checked-images' style='display: none;'>Remove images</button>")

              //if the next picture is 1 second after then only the images will be
              //exchanged and the video will stay minimized
              //example picture ends at 40 seconds and the next image starts at 41
              //$startIndex must be larger than 0 so the animation can start
              var startIndex = 0;
              var endIndex = 0;
              var conIndex = false;
              var currentImg = 0;
              var check = 0;
              $("#video").on(
                "timeupdate",
                function() {
                  //check is set to false as it always needs to be set to true for
                  //each change in the current time update
                  check = false;
                  for(var i = 1; i < (videoImages.length + 1); i++) {
                      if (currentTime >= videoImages[i - 1][0] && currentTime <= videoImages[i - 1][1]) {
                          currentImg = i;

                          // enshures that the correct image is shown by cheking if the url of the current img
                          // is equal to the src of the current image
                          if($('#large-video-image').attr('src') != videoImages[currentImg - 1][2]) {
                            $('#large-video-image').attr('src', videoImages[currentImg - 1][2]);

                            $("#" + imageDivIds[currentImg - 1]).css({
                              borderColor: "green",
                              borderSize: "1px",
                              borderStyle: "solid"
                            });
                          }
                          //il explain later
                          check = true;
                        }
                      }

                      if(tmpCurrentImg != currentImg) {
                        //removes style from currentImg
                        $("#" + imageDivIds[(tmpCurrentImg - 1)]).css({
                          borderColor: "black"
                        });
                      }

                      tmpCurrentImg = currentImg;

                      if(check != true) {
                        currentImg = 0;
                      }

                      if(currentImg != 0 && conIndex == false) {
                        //minimize video
                        //console.log(currentImg);
                        $('#video').css({position: 'absolute'})
                        moveVideoWindow("rightbottom");
                        $('#large-video-image').css({display: "inline"});
                        $('#large-video-image').attr('src', videoImages[currentImg - 1][2]);
                        conIndex = true;

                        $("#" + imageDivIds[currentImg - 1]).css({
                          borderColor: "green",
                          borderSize: "1px",
                          borderStyle: "solid"
                        });
                      } else if (conIndex == true && currentImg == 0) {
                        $('#large-video-image').css({display: "none"});
                        moveVideoWindow("lefttop");
                        conIndex = false;
                      } else if (conIndex == true && currentImg == 0) {}
            }); //ontime updade


            $('.video-image').click(function() {
              //alert($(this).attr("src"));
              for(var i = 0; i < videoImages.length; i++) {
                if(videoImages[i][2] == $(this).attr("src")) {
                  document.getElementById('video').currentTime = videoImages[i][0];
                }
              }
            });

            $('#edit').click(function() {
              if($('#edit').is(':checked')) {
                for(var i = 0; i < imageCheckboxIds.length; i++) {
                  $('#' + imageCheckboxIds[i]).css("display" , "block");
                  $('#remove-checked-images').css("display" , "block");
                }
              } // on edit checked
              else {
                for(var i = 0; i < imageCheckboxIds.length; i++) {
                  $('#' + imageCheckboxIds[i]).css("display" , "none");
                }
              } // on edit unchecked
            }); //on edit click

            //removes the images checked from the
            $("#remove-checked-images").click(function() {
              var imgData = new Array();
              var t = 0;
              for(var i = 0; i < imageCheckboxIds.length; i++) {
                if ($('#' + imageCheckboxIds[i]).is(':checked')) {
                  imgData[t] = $('#' + imageCheckboxIds[i]).val();
                  t++;
                }
              }

              console.log(imgData);

              $.ajax({
                context: this,
                type: 'POST',
                url: 'ajax.php',
                data: {action : 'IMAGE_DELETE', imgData : imgData},
                success: function(response) {
                  alert(response);

                  for(var i = 0; i < imageCheckboxIds.length; i++) {
                    if ($('#' + imageCheckboxIds[i]).is(':checked')) {
                      $('#' + imageDivIds[i]).css("display" , "none");
                      videoImages.splice(i, 1);
                    }
                 } //for loop
               } //on success
             }); //ajax
           }); //on click
         } //else if response from first ajax was not 404
       //on success
     }, error :function(response) {
       console.log('fail');
     }
   }); //ajax
 }





    //http://stackoverflow.com/questions/6380956/current-duration-time-of-html5-video
    videoPlayer.addEventListener('loadeddata', function() {
      $('#duration').text(videoPlayer.duration);
      $("#video").on(
        "timeupdate",
        function(){
          currentTime = this.currentTime;
          $("#current-time").text(currentTime);
        });
      }, false);
      /*

      ('#newVideoSubmitBtn').click(function(event) {
var title1 = $('#newVideotitleInput').val();								//TODO validation
var description1 = $('#newVideoDescriptionInput').val();

var formData = new FormData();
formData.append('fileToUpload', $('#newVideoFileInput').prop('files')[0]);

for (i = 0; i < $('#subtitlesUploadDiv input').length; i++) {
  formData.append('subtitleFiles[' + i + ']', $('#subtitlesUploadDiv input')[i].files[0]);
}

for (i = 0; i < $('#subtitlesUploadDiv select').length; i++) {
  formData.append('languages[' + i + ']', $('#subtitlesUploadDiv select').eq(i).val());
}


formData.append('uid', profileId);
formData.append('title', $('#newVideotitleInput').val());
formData.append('description', $('#newVideoDescriptionInput').val());

$("#uploadInProgress").show();


$.ajax({
  context: this,
  url: 'upload.php',
  data : formData,
      processData: false,  // tell jQuery not to process the data
      contentType: false,  // tell jQuery not to set contentType
      type: 'POST',
})
.done(function(data) {
  console.log("successnewvideo");
  getVideosFromServer(profileId);
  console.log(data);
})
.fail(function() {
  console.log("errornewvideo");
})
.always(function(data) {
  console.log("complete");
  $("#uploadInProgress").hide();
});
});

      */

      //Taken from http://jsfiddle.net/rws95a1q/4/
      function moveVideoWindow(moveTo) {
            /*else if (moveTo === "lefttop") {
            videoDivWindow.stop().animate({
            bottom: windowHeight - videoDivHeight
            , right: windowWidth - videoDivWidth
          }, 5000);
        }*/
        if (moveTo === "rightbottom") {
          $('#video').css({position: 'fixed'})
          videoDivWindow.stop().animate({
            bottom: 0,
            right: 0,
            width: 200
          }, "fast");
        }
        else if (moveTo === "lefttop") {
          videoDivWindow.stop().animate({
            bottom: windowHeight - videoDivHeight,
            right: windowWidth - videoDivWidth,
            width: 960
          }, "fast", function() {
            videoDivWindow.css({position: 'static'})
          });
          /*else {
          chatWindow.stop().animate({
          bottom: 0
          , right: 0
        }, 2000);
        */
        }
      }
