
<?php
  session_start();
  require_once 'include/db.php';    // Connect to the database
  require_once 'classes/user.php';
  require_once 'classes/videoInterface.php';

  $id = $_POST['id'];
  $isPlaylist = $_POST['isPlaylist'];
/*

	require_once 'header.php';

		// ajax result that ads current video to the selected playlist
    if (isset($_POST['action']) && !empty($_POST['action'])) {
        if ($_POST['action'] == 'ADD_VIDEO_TO_PLAYLIST') {
            $videoInterface->addVideoToPlayList($_POST['playlistId'], $_POST['videoId']);
            echo "added video to playlist";
        }
        else {
            echo "failed";
        }

    }
*/
?>



  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/global.css">
  <link rel="stylesheet" href="css/watch.css">

  <style type="text/css" media="screen">


  #images-studentlist-div {
    clear: both;
    height: auto;
    width: 1360px;
    margin-top: 10px;
    border: 1px solid gray;
  }

  /*VIDEO IMAGES*/


  #large-video-image {
    display: none;
    width: 960px;
    height: 540px;
    border: 1px solid #B1B1B1;
    border-right: none;
  }

  #edit-label {
    display: none;
  }

  .video-image-container {
    float: left;
    height: auto;
    border-bottom: 5px solid grey;
    width: 210px;
    padding: 5px;
    margin: 5px;
  }

  #video-image-upload-div {
    display: none;
  }

  .video-image {

  }

  /* The switch - the box around the slider */
  .switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
  }

  /* Hide default HTML checkbox */
  .switch input {display:none;}

  /* The slider */
  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
  }

  .slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
  }

  input:checked + .slider {
    background-color: #2196F3;
  }

  input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
  }

  input:checked + .slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
  }

  /* Rounded sliders */
  .slider.round {
    border-radius: 34px;
  }

  .slider.round:before {
    border-radius: 50%;
  }

  .checkbox-image {
    padding: 10px 0px 10px 10px;
    margin: 0;
    display: none;
  }

  #videoAndTextContainer {
    margin: 0px;
    padding: 0px;
    width: 1360px;
    height: 600px;
    border: none !important;
  }

  #videoFrame {
    display: block;
    margin: 0px;
    padding: 0px;
    float: left;
  }

  .contentContainer {
    height: 100vh;
  }

  .pre-scrollable {
    max-height: 100%;
    overflow-y: scroll;
  }

  #addToPlayListContainer {
    float: left;
  }

  #descriptionDiv {
    margin: 10px 0px;
    margin-bottom: 35px;
    width: 960px;
  }

  .autoplayBtnOff {
    opacity: 0.3;
  }

  #autoplayBtn {
    float: right;
  }

  #extraControls {
    margin: 0px;
    padding: 0px;
    width: 960px;
    height: 37px;
    background: #E1E1E1;
  }

  #extraControls .btn {
    border:  none !important;
  }

  #video {
    width: 960px;
    height: 540px;
    border: 1px solid #B1B1B1;
    border-right: 0px;
    padding: 0;
    margin: 0px;
    padding: 0px;
    z-index: 1;
    background-color: black;
  }



/* PLAYLIST CSS START*/
    #playlistContainer {
      width: 900px;
      float: left;
      margin-bottom: 50px;
    }

    #playlistContainer i {
      float: left;
      position: relative;
      top: 26px;
    }

    #playlistContainer img {
      width: 160px;
      height: 90px;
    }

    #playlistVideosContainer {
      width: 812px;
      height: 94px;
      border: 1px solid black;
      float: left;
      margin: 0px 15px;
    }

    #playlistVideosContainer li {
      float: left;
      border: 1px solid black;
      width: 162px;
      height: 92px;
      margin: 0;
      padding: 0;
      list-style-type: none;
    }

    #playlistVideosContainer ul {
      float:  left;
      margin: 0;
      padding: 0;
    }


    .activeVideoInPlayList {
      -webkit-transform: scale(1.1);
          -ms-transform: scale(1.1);
          transform: scale(1.1);
          transition: 0.4s ease;
    }

    .faded-arrow {
      opacity: 0.2;
    }

    .hidden-video {
      display: none;
      transition: 0.4s ease;
    }



    /**SUBS CSS*/
    #textDiv{
      background: #F4F4F4;
      padding: 2px 10px;
      background: ;
      border: 1px solid #B1B1B1;
      border-left: none;
      width: 400px;
      height: 540px;
      float: right;
      overflow-y: scroll
    }

    .highlight {
      color: blue;
      text-decoration: underline;
    }

    #languageSelect {
      margin: 0px;
      padding: 0px;
      width: 100px;
    }

    #speedSelect ul {
      height: 300px;
    }

    .dropdown {
      float: left;
    }

/*RATINGS*/

		#ratingContainer {
			width: 120px;
			margin-top: 8px;
			float:left;
		}

		#ratingContainer i {
			margin: 2px;
			float:left;
		}

		.activeStar {
			color:yellow;
		}

		#rateItText {
			margin: 5px 7px 0px 80px;
			float:left
		}














	#editSubDiv .topDiv{
	  width: inherit;
	  clear: both;
	}
	#editSubDiv .rightSide{
	 float: right;
	 margin: 5px;

	}

	#editSubDiv .rightSide input{
	  width: 50px;
	  margin-right: 3px;

	}

	#editSubDiv .rightSide button{
	  margin-right: 5px;
	}

	#editSubDiv .leftSide{
	  float: left;
	  width: 600px;
	  padding-left: 10px;

	}

	 #newTextEditDiv textarea{
	  resize: none;
	  height: inherit;
	  width: inherit;
	  overflow-y: scroll;

	 }
	 #addSubLineDiv button{
	  margin-top: 10px;
	  margin-left: 29px;
	 }


	 #newTextEditDiv{
	  height: 100px;
	 }

	 #newTimeEditDiv{
	  height: 100px;
	  float: right;
	 }

	 #editSubDivCollapse{
	    margin-top: 10px;
	    border: 1px solid black;
	    height: 700px;
	    width: 960px;
	    background-color: #F4F4F4;

	}
	  #addSubLineDiv{
	    width:inherit;
	    padding: 20px;
	    height: 200px;
	    border-top: 1px solid black;
	  }
	  #subEditCollapseButton{
	    margin-top: 20px;
      margin-left: 5px;
      display: none;
	  }

	  #editSubDiv{
	    height: 500px;
	    width: 99,9%;
	    overflow-y: scroll;

	  }

    #studentListButton {
      clear: both;
      padding-top: 50px;
      margin-left: 3px;
    }

    #studentListCollapse {
      margin-bottom: 200px;
      margin-top: 10px;
      padding: 5px;
      width: 400px;
      border: 1px solid gray;
      background-color: #F4F4F4;
      clear: both;
    }

    #studentListDiv {
      min-height: 200px;
      min-width: 400px;
      width: 400;
      clear: both;
    }

  </style>

<div>
    <div class="container-fluid contentContainer">




     <?php

    	echo "<h2>".$videoInterface->getVideoById($id)['title']."</h2>";

    ?>


    <div class="container-fluid" id="videoAndTextContainer">

          <?php
					echo "<video id='video' data-id='". $id ."' controls preload='metadata' data-lang='en'>";


            echo  "<source src='".$videoInterface->getVideoById($id)['filePath']." 'type='video/mp4'>";

            $subtitles = $videoInterface->getSubtitlesByVideoId($id);
            foreach ($subtitles as $key =>  $subtitle) {
              echo  "<track label='".$subtitle['language']."' kind='subtitles' srclang='".$subtitle['languageCode']."' src='".$subtitle['filePath']."' default>";
            }

            ?>
         </video>

          <img id=large-video-image src="" alt="large-video-image" type="img/jpg">

          <div id="textDiv">

          </div>


       <div id='extraControls'>
          <div class="dropdown">
            <a class="btn btn-secondary dropdown-toggle" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Language
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" id="languageSelect">
              <ul class="list-group">

              <?php
							//gets each subritle for a video
              foreach ($subtitles as $key => $subtitle) {
                  if ($key == 0) {
                    echo  "<li class='list-group-item active' data-lang='".$subtitle['languageCode']."'>".$subtitle['language']."</li>";
                  }
                  else {
                    echo  "<li class='list-group-item' data-lang='".$subtitle['languageCode']."'>".$subtitle['language']."</li>";
                  }
              }

              ?>
              </ul>
            </div>
          </div>

          <div class="dropdown">
            <a class="btn btn-secondary dropdown-toggle" href="https://example.com" id="dropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Speed
            </a>

						<!-- lists different items in the select video speed in a dropdown-menu-->
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink2" id="speedSelect">
              <ul class="list-group">
                  <li class="list-group-item">0.25</li>
                  <li class="list-group-item">0.5</li>
                  <li class="list-group-item">0.75</li>
                  <li class="list-group-item active">1</li>
                  <li class="list-group-item">1.25</li>
                  <li class="list-group-item">1.5</li>
                  <li class="list-group-item">1.75</li>
                  <li class="list-group-item">2</li>
                  <li class="list-group-item">2.5</li>
                  <li class="list-group-item">3</li>
              </ul>
            </div>
        </div>
        <div id="addToPlayListContainer">
          <div class="dropdown">
            <a class="btn btn-primary dropdown-toggle" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Add to playlist
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

            <?php
              if (isset($_SESSION['uid'])) {
								 // get the playlists by user id
                  $playlists = $videoInterface->getPlaylistsByUserId($_SESSION['uid']);
                  foreach ($playlists as $key => $value) {
                      echo "<a class='dropdown-item playlist-choice' data-videoId='". $id ."' data-playlistId='".$value['id']."' href='#'>".$value['title']."</a>";
                  }
              }

            ?>

          </div>
        </div>
      </div>

			<span id="rateItText">Rate it!</span>
			<div id="ratingContainer">
				<?php
				// get the current rating for the video. Value will be users rating of the
				// video, if user has not rated the video the rating will be set to the average
				// rating
				$rating = round($videoInterface->getRating($id));
					for ($i = 1; $i <= 5; $i++){
						if ($i <= $rating) {
							echo "<i class='fa fa-star activeStar' value='".$i."' aria-hidden='true'></i>";
						}
						else {
							echo "<i class='fa fa-star' value='".$i."' aria-hidden='true'></i>";
						}
					}
				 ?>
			</div>

      <button type="button" class="btn btn-success" id="autoplayBtn">Autoscroll</button>


			</div>

   </div>






<div class='row' id='subEditCollapseButton'>
   <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#editSubDivCollapse" aria-expanded="false" aria-controls="editSubDivCollapse">
    Edit Sub
  </button>
 </div>

  <div class="collapse" id="editSubDivCollapse">
    <div id="editSubDiv">

    </div>
      <div id="addSubLineDiv" >
        <div id="newTimeEditDiv" class="col-md-3">


         <div class="form-group row">
      <label  class="col-sm-2 col-form-label">Start</label>
      <div class="col-sm-10">
        <input type="text" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label  class="col-sm-2 col-form-label">End</label>
      <div class="col-sm-10">
        <input type="text" class="form-control">
      </div>
    </div>
 </div>
        <div id="newTextEditDiv" class="col-md-9">
          <textarea>

          </textarea>

        </div>
         <div class="form-group row">
        <button type="buttom" class="btn btn-primary">Add Line</button>
        </div>
      </div>
    </div>


    <h4 style="margin-top: 20px;">Description: </h4>
      <div id="descriptionDiv">
      <?php
			  //get all the video information
        echo $videoInterface->getVideoById($id)['description'];
      ?>
      </div>



      <?php
        if (isset($_POST['plId'])) {
          echo "<div class='container-fluid'>
                 <div id='playlistContainer'>
                  <i class='fa fa-arrow-left fa-2x' aria-hidden='true'></i>
                  <div id='playlistVideosContainer'>
                    <ul>";

										//get all videos in the selected playlist
                     $videos = $videoInterface->getVideosByPlaylistId($_POST['plId']);
                    foreach ($videos as $key => $video) {
                      if ($key == 0) {
                        echo "<li class='activeVideoInPlayList' data-rating='".$video['rating']."' data-id='".$video['id']."' data-title='".$video['title']."' data-description='".$video['description']."' data-filePath='".$video['filePath']."'><img src='https://fbnewsroomus.files.wordpress.com/2015/04/messenger-video-call-carousel-icon.png?w=960'><h6>".$video['title']."</h6></li>";
                      }
                      else {
                        echo "<li data-rating='".$video['rating']."' data-id='".$video['id']."' data-title='".$video['title']."' data-description='".$video['description']."' data-filePath='".$video['filePath']."'><img src='https://fbnewsroomus.files.wordpress.com/2015/04/messenger-video-call-carousel-icon.png?w=960'><h6>".$video['title']."</h6></li>";
                      }
                    }

                echo"</ul>
              </div>
              <i class='fa fa-arrow-right fa-2x' aria-hidden='true'></i>
            </div>

          </div>  ";
        }?>

    <div id="images-studentlist-div">

    <div id="video-image-upload-div">
     <form enctype="multipart/form-data" id="newVideoImageForm">
      Select image to upload and select the start and en time for the image:
      <input type="file" name="fileToUpload" id="fileToUpload">
      <input type="text" name="startTime" id="videoStartTime" placeholder="start time">
      <input type="text" name="endTime" id="videoEndTime" placeholder="end time">
      <button type="button" id="newVideoImageSubmitBtn" value="Upload Image">Save image</button>
     </form>
    </div>

    <div id="video-images">
      <label id="edit-label" class="switch">
        <input id="edit" type="checkbox">
        <div class="slider round"></div>
      </label>
      <div id="images-container-div">

      </div>
    </div>

  </div>

    <?php if($user->getAdmin()) { echo "<div id='studentListDiv'>" .
      "<div class='row' id='studentListButton'> " .
         "<button class='btn btn-secondary' type='button' data-toggle='collapse' data-target='#studentListCollapse' aria-expanded='false' aria-controls='studentListColapse'> ".
          "Show students watched list" .
        "</button>".
        "</div>".
        "<div class='collapse' id='studentListCollapse'>".
        " <div id='student-list'>".
          "</div>".
        "</div>".
      "</div>"; } ?>
     <!-- <script type='text/javascript' src='js/watch.js'></script> -->

    <script type="text/javascript" charset="utf-8" async defer>

    	 $(".playlist-choice").click(function(event) {

        $.ajax({
                context: this,
                url: 'ajax.php',
                type: 'POST',
                data: {action: 'ADD_VIDEO_TO_PLAYLIST', playlistId: "" + $(this).attr('data-playlistId'), videoId: "" + $(this).attr('data-videoId')},
            })
            .done(function() {

            })
            .fail(function() {
                //alert("error");
            })
            .always(function() {
                console.log("blalalalalaa");
            });

    });


    $("#newVideoImageSubmitBtn").click(function(event) {
           console.log('pressed');


           console.log(""+ $('#video').attr('data-id'));
           console.log(<?php echo $_SESSION['uid'];?>);

           var formData = new FormData();

           formData.append('fileToUpload', $('#fileToUpload').prop('files')[0]);
           formData.append('startTime', $('#videoStartTime').val());
           formData.append('endTime', $('#videoEndTime').val());
           formData.append('videoId', $('#video').attr('data-id'));
           formData.append('userId', "<?php echo $_SESSION['uid'];?>");

           console.log(formData);
           //formData.append('uid', profileId);
           //formData.append video id
           $.ajax({
             context: this,
             url: 'upload-video-image.php',
             data : formData,
                 processData: false,  // tell jQuery not to process the data
                 contentType: false,  // tell jQuery not to set contentType
                 type: 'POST',
                 success: function(response){
                   console.log(response);

                   $('#images-container-div').empty();
                   getImageData();


                 }, error: function(response) {
                   console.log('error');
                 }
           });
        });

    //get the images that belongs to the picture
    //  getImageData();


    $(".playlist-choice").click(function(event) {

        $.ajax({
                context: this,
                url: 'watch.php',
                type: 'POST',
                data: {action: 'ADD_VIDEO_TO_PLAYLIST', playlistId: "" + $(this).attr('data-playlistId'), videoId: "" + $(this).attr('data-videoId')},
            })
            .done(function() {

            })
            .fail(function() {
                //alert("error");
            })
            .always(function() {

            });

    });

    // playlist js start


      // the playlist is 5 videos in length visually, if we have less videos than that,
      // we wanto to shrink the size of the playlist and diable the arrows as there is nothign to navigate to.
      var nrOfVideosInplaylist = $("#playlistVideosContainer li").length;
      if (nrOfVideosInplaylist < 5) {
        var newContainerwidth = (nrOfVideosInplaylist % 5) * 162 + 2;
        $("#playlistVideosContainer").css('width', "" + newContainerwidth + "px");
        $("#playlistContainer .fa-arrow-right").addClass('faded-arrow');
      }

      // we only want to show 5 videos in the list at a time, so to start we hide every video except the 5 first.
      else if (nrOfVideosInplaylist > 5) {
        $("#playlistVideosContainer li:gt(4)").addClass('hidden-video');
      }

      else {
        $("#playlistContainer .fa-arrow-right").addClass('faded-arrow');
      }

      // if there is videos in the list, we set the title, src and description etc.
      if (nrOfVideosInplaylist > 0) {
          var firstVideo = $("#playlistVideosContainer li").first();
          loadVideofromPlaylist(firstVideo);
        }

      // the left arrow button will always be diabled at first.
      $("#playlistContainer .fa-arrow-left").addClass('faded-arrow');

      // when a video is clicked it is selected by adding the activeVideoInPlayList. the presvious selected video is
      // had this class removed. We set the source in the video player to play this video. we also set title, description etc.
      $("#playlistVideosContainer li").click(function(event) {
        loadVideofromPlaylist($(this))
      });

      //clicking the right arrow we have to push every video 1 step to the left. we do this by hiding the first visible element, and showing
      //the first hidden element that comes after it.
      $("i.fa-arrow-right").click(function(event) {
        var nextVideo = $("#playlistVideosContainer li:not(.hidden-video)").last().next();
        if (nextVideo.hasClass('hidden-video')) {
          $("#playlistVideosContainer li:not(.hidden-video)").first().addClass('hidden-video');
          nextVideo.removeClass('hidden-video');
          // fades the arrow if there is no more videos to the right.
          if(nextVideo.is(':last-child')) {
            $("#playlistContainer .fa-arrow-right").addClass('faded-arrow');
          }
        }
        if (nrOfVideosInplaylist > 5) {
          $("#playlistContainer .fa-arrow-left").removeClass('faded-arrow');
        }
      });


      // same here but for left button
      $("i.fa-arrow-left").click(function(event) {
        var nextVideo = $("#playlistVideosContainer li:not(.hidden-video)").first().prev();
        if (nextVideo.hasClass('hidden-video')) {
          $("#playlistVideosContainer li:not(.hidden-video)").last().addClass('hidden-video');
          nextVideo.removeClass('hidden-video');

          if(nextVideo.is(':first-child')) {
            $("#playlistContainer .fa-arrow-left").addClass('faded-arrow');
          }
        }
        if (nrOfVideosInplaylist > 5) {
          $("#playlistContainer .fa-arrow-right").removeClass('faded-arrow');
        }
      });


      // when a video finishes we want to play the next in the list if there is one.
      $("#video").bind("ended", function() {
        var nextVideo = $(".activeVideoInPlayList").next('li');
        if (nextVideo.attr('data-id') != null) {
           loadVideofromPlaylist(nextVideo); // if its not the last movie, we load the next.
        }
      });


  // This function takes in a li for a video in the playlist. is sets this video to be the active one
  // and loads the title, description and src etc for the film. we then play the new video
  function loadVideofromPlaylist(videoAsLi) {
    $('#playlistVideosContainer .activeVideoInPlayList').removeClass('activeVideoInPlayList');
    videoAsLi.addClass('activeVideoInPlayList');
    var filePath = videoAsLi.attr('data-filePath');
    $("#video").attr('src', filePath);
    $("h2").html(videoAsLi.attr('data-title'));
    $("#video").attr('data-id', videoAsLi.attr('data-id'))
    $("#descriptionDiv").html(videoAsLi.attr('data-description')); // this one is used to easily add a video to a new playlist

    $("#ratingContainer .fa-star").removeClass('activeStar');
    var videoRating = Math.round(parseInt(videoAsLi.attr('data-rating')));
    $("#ratingContainer .fa-star:lt(" + videoRating + ")").addClass('activeStar');

    loadSubtitleTracksForVideo(videoAsLi.attr('data-id'));

    $("#video")[0].play();


  }







//SUBS START


   $( document ).ready(function() {
  var video = document.getElementById('video');  // Finding the video
  video.play();                                  // Starting the video
  addListeners(video);                       // adding listeners for video events
  setTimeout(function(){
    loadTrack($("video").attr("data-lang"));                           // loading the active text track
  }, 100);

                                             // Changing the text track when a link in
                                             // the dropdown is clicked
  $("#textDiv div#languageSelect > ul > li.available").on('click', 'a' ,changeTrack);
                                             // changing the video time and playing
  $("#textDiv").on('click','p',function(){   // video when a line of thext is clicked
    video_settime($(this).data("start"));
    video.play();
  });


  addLanguageSelectClickListeners();


 $("#speedSelect li").click(function(event) {
    if ($(this).hasClass('active')) {
      return;
    }
    $("#speedSelect li").removeClass('active');
    $(this).addClass('active');
    document.querySelector("#video").playbackRate = $(this).html();
  });



});
// Updating the language
// that is set in the dropdown
// and is giving the new language
// the class active
function changeTrack(){
  event.preventDefault();

  var lang = $('#video').attr("data-lang");

  $("#textDiv div#languageSelect > ul > li.active").each(function(){
    $(this).removeClass("active-track")
  })
  $("div#languageSelect button#languageButton span.lang-sm").attr("data-lang", lang);

  $("#textDiv div#languageSelect > ul > li."+lang).addClass("active");

  loadCurrent();
}


function loadSubtitleTracksForVideo (vidId) {
  $.ajax({
          context: this,
          url: 'ajax.php',
          dataType: "json",
          type: 'POST',
          data: {action: 'GET_TRACK_HTML', videoId: "" + vidId},
      })
      .done(function(data) {

          $("#languageSelect ul").html("");
          $("video").html("");


          $("#video").html(data['trackHtml']);
          $("#languageSelect ul").html(data['languageSelectHtml']);

          $("video").attr("data-lang", "");
          $("video").attr("data-lang", $("#languageSelect li.active").attr("data-lang"));


          $("#textDiv").html("");
          addLanguageSelectClickListeners();


          loadTrack($("video").attr("data-lang"));

          setTimeout(triggerClick, 200);



      })
      .fail(function() {
          //alert("error");
      });
}

function triggerClick() {
  console.log("aa");
  $("#languageSelect li.active").trigger("click");
}


// Adding listeners for when a video is changing time
// and when the vieo is finished
function addListeners(video) {
 video.addEventListener('timeupdate', update, false);
 video.addEventListener('ended', nextvideo, false);
}


function addLanguageSelectClickListeners() {
  $("#languageSelect li").click(function(event) {

    $("#languageSelect li").removeClass('active');
    $(this).addClass('active');
    $("video").attr("data-lang", $(this).attr("data-lang"));
    loadTrack($("video").attr("data-lang"));

  });
}

// Looking for a link in the playlist div with the class next.
// If it is found user will be sent there
function nextvideo(event){
  if($('#playlist a.next').attr('href')){
    window.location.href=$('#playlist a.next').attr('href');
  }
}

// when the video time is changing it will chack if there is a text
// line that should be marked as the current one giving the
// row class hightligth if it is at the right time removing the higlight
// class if it is no longer the correct time
function update(event){
  $('#textDiv').find('p').each(function(index) {
    var row = $(this);
    var start = row.data('start');
    var end =   row.data('end');
    if(parseFloat(start) <= parseFloat(event.target.currentTime) && parseFloat(end) >= parseFloat(event.target.currentTime) ){
      row.addClass('highlight');
    }else{
      row.removeClass('highlight');
    }
  });


  // here we scroll the subtitle text depending on the position relative to the top
  //of the viible textarea, so the highlighted text is always just slightly above the center

  if (!$("#autoplayBtn").hasClass('autoplayBtnOff')) {
    var currentText = $("#textDiv p.highlight");

    if (currentText.length != 0) {
      var curPos = $("#textDiv p.highlight").position().top;

      if (curPos < 240) {
        var currentScroll = $("#textDiv").scrollTop();
        $("#textDiv").animate({ scrollTop: currentScroll + curPos - 240});
      }

      if (curPos > 240) {
        var currentScroll = $("#textDiv").scrollTop();
        //console.log(currentScroll);
        $("#textDiv").animate({ scrollTop: currentScroll + curPos - 240});
       }

       else if (curPos < 0) {

       }
     }

  }

}

// decides wwteher autoscroll is on or the user can freely scroll
$("#autoplayBtn").click(function(event) {
    $(this).toggleClass('autoplayBtnOff');
});




// setting the time of the video to a new time
function video_settime(newtime){
  var video = document.getElementsByTagName("video")[0];
  video.currentTime = newtime;
}

// reading the ques from the track and are displaying the textin the textDiv
function readContent(track) {
  $("#textDiv").html("");
  var cues = track.cues;
  for(var i=0; i < cues.length; i++) {
    var cue = cues[i];

    var id    = cue.id ;
    var start = cue.startTime;
    var end   = cue.endTime;
    var text  = cue.text ;
    var rowData = "<p data-start='" + start + "' data-end='" + end + "'>" + text + "</p>"
    $("#textDiv").append(rowData);
  }
    loadToSubArry();
    console.log("load");
}

// getting a video, and loading the video with the language that was
// send as a parameter
function loadTrack(lang) {
  console.log(lang);
  var video = document.getElementById('video');
  for (var i = 0; i < video.textTracks.length; i++) {
    if (video.textTracks[i].language == lang) {
      video.textTracks[i].mode = 'showing';
      readContent(video.textTracks[i]);
    }else {
      video.textTracks[i].mode = 'hidden';
    }
  }
  //get the images that belongs to the picture
  getOwnerId();
  getImageData();
  addStudentToWatchlist();
  getStudentWatchedList();
}


$("#ratingContainer .fa-star").click(function(event) {
  $("#ratingContainer .fa-star").removeClass('activeStar');
  $(this).addClass('activeStar');
  var curSelectedStar = $(this).attr('value');
  var videoId = $("#video").attr('data-id');
  $("#ratingContainer .fa-star:lt(" + curSelectedStar + ")").addClass('activeStar');
  $.ajax({
          context: this,
          url: 'ajax.php',
          type: 'POST',
          data: {action: 'RATE_VIDEO', rating: "" + curSelectedStar, videoId: "" + videoId},
      })
      .done(function(data) {

      })
      .fail(function() {
          alert("error");
      })
      .always(function() {

      });
});




  var lineArr = [];



  $("#editSubDiv").on('click', '.saveBtn', function(event){
      var startinput = $(this).siblings('.startspan').children('input').val();
      var endinput = $(this).siblings('.endspan').children('input').val();

      if(startinput > 0 && startinput < endinput){
         var index = $('#editSubDiv').children().index($(this).closest('.topDiv'));
         lineArr[index]['start'] = startinput;
         lineArr[index]['end'] = endinput;
         var o = lineArr.splice(index, 1)[0];

         for (i = 0; i < lineArr.length; i++) {
              if(parseFloat(startinput) < parseFloat(lineArr[i]['start'])){
                  lineArr.splice(i, 0, o);
                  console.log(lineArr);
                  displayEditSubText(lineArr);
                  WriteSubtile();
                  saveSubToServer();
                  return;
              }
          }
          lineArr.push(o);
          displayEditSubText(lineArr);
          WriteSubtile();
          saveSubToServer();
      }
  });



  $("#editSubDiv").on('click', '.deleteBtn', function(event){
    var index = $('#editSubDiv').children().index($(this).closest('.topDiv'));
    lineArr.splice(index, 1);
    displayEditSubText(lineArr);
    WriteSubtile();
    saveSubToServer();

  });

 function WriteSubtile(){
    var video = document.getElementById('video');
    for( i = 0; i < video.textTracks.length; i++){
      if(video.textTracks[i].language == video.getAttribute('data-lang')){
          var sTrack = video.textTracks[i];
          var cues = sTrack.cues;
          for(k = 0; k < lineArr.length; k++){
             if(k >= cues.length){
                cues[k] = cues[0];

                cues[k].startTime = lineArr[k]['start'];
                cues[k].endTime = lineArr[k]['end'];
                cues[k].text =  lineArr[k]['text'];

             }
             else{
                var cue = cues[k];
           //  console.log(cue.text);
                cue.startTime = lineArr[k]['start'];
                cue.endTime = lineArr[k]['end'];
                cue.text =  lineArr[k]['text'];
            }
        }
          for(j = lineArr.length; j < cues.length; j++){
            cues[j].text = "";
          }
          readContent(sTrack);
      }
      else{
          video.textTracks[i].mode='hidden';
      }
    }
 }


 function loadToSubArry() {
  lineArr = [];
   for (i = 0; i < $("#textDiv p").length; i++){
          var p = $('#textDiv p').eq(i);
          lineArr[i] = {'start': p.attr('data-start'), 'end': p.attr('data-end'), 'text' :p.text()};
    }
    displayEditSubText(lineArr);
}

  function displayEditSubText(lineArray){
      console.log("load-display");
      console.log(lineArray);
     $('#editSubDiv .topDiv').remove();
    for (i = 0; i < lineArray.length; i++){
      $('#editSubDiv').append(
        "<div class='topDiv'>" +
            "<div class='leftSide'>" +
                "<span class='textspan'>" +
                      lineArray[i]['text'] +
                "</span>" +
            "</div>" +
            "<div class='rightSide'>" +
                "<span class='startspan'>" +
                    "<input type='text' value='" + lineArray[i]['start']  + "'>" +
                "</span>" +
                "<span class='endspan'>" +
                    "<input type='text' value='" + lineArray[i]['end']  + "'>" +
                "</span>" +
                "<button type='button' class='btn btn-primary saveBtn'>" +
                    "save" +
                "</button>" +
                "<button type='button' class='btn btn-danger deleteBtn'>" +
                    "delete" +
                "</button>" +
            "</div>" +
        "</div>");
    }
    if($('track').length <= 0){
      $('#subEditCollapseButton').hide();
    }
  }


  function saveSubToServer(){
    var subPath = $('#video track').attr('src');
    $.ajax({
          context: this,
          url: 'updateSub.php',
          type: 'POST',
          data: {subPath: subPath, subArray: lineArr},
      })
      .done(function(data) {
          loadTrack($("video").attr("data-lang"));
          console.log(data);
      })
      .fail(function() {
          console.log("error");
      })
      .always(function() {
          console.log("complete");
      });


  }


  $("#addSubLineDiv button").click(function() {
    var text = $("#newTextEditDiv textarea").val();
    var start = $("#addSubLineDiv input").eq(0).val();
    var end = $("#addSubLineDiv input").eq(1).val();
    console.log(lineArr.length);
    for(i = 0; i < lineArr.length; i++){
      var startTime = lineArr[i]['start'];
      if(parseFloat(startTime) > parseFloat(start)){
        lineArr.splice(i, 0, {'start': start, 'end': end, 'text': text});
        displayEditSubText(lineArr);
        WriteSubtile();
        saveSubToServer();
        return;
      }
    }
    lineArr.push({'start': start, 'end': end, 'text': text
  });
    WriteSubtile();
    saveSubToServer();
    displayEditSubText(lineArr);


  });

/*if($('track').length <= 0){
  $('#subEditCollapseButton').hide();
}*/

var videoPlayer = document.getElementById("video");

var duration = 0;
var currentTime = 0;

var videoDivWindow = $("#video");
var videoDivWidth = videoDivWindow.width();
var videoDivHeight = videoDivWindow.height();

var windowWidth = $(window).width();
var windowHeight = $(window).height();

var videoImages = new Array();
var tmpCurrentImg = 0;

var imageIds = Array();
var imageDivIds = Array();
var imageCheckboxIds = Array();
var imageCheckboxDivIds = Array();

//A call that will get all the image data for the video

function getImageData() {
var videoId = $('#video').attr('data-id');
console.log('sadsd');
$.ajax({
  context: this,
  type: 'POST',
  url: 'ajax.php',
  data: {action: 'GET-VIDEO-IMAGE-DATA' , videoId: "" + videoId},
  dataType: 'json',
  success: function(response) {
          console.log(response);
            $('#images-container-div').empty();
            //console.log(response);
            videoImages = response;
            $('#images-container-div').append("<div>");
            //append to the images-container-div elements that will show the images information and other relevant functionality
            for(var i = 0; i < videoImages.length; i++) {
              console.log(i);
              imageIds[i] = "video-img-div-" + i;
              imageDivIds[i] = "video-img-" + i;
              imageCheckboxIds[i] = "video-image-checkbox-" + i;
              imageCheckboxDivIds[i] = "video-image-checkbox-div-" + i;
              //sets up a div for each image
              $('#images-container-div').append("<div id='" + imageDivIds[i] + "' class='video-image-container'></div>");
              //sets up a div for each checkbox that wil be used to select the image to delete it
              $('#' + imageDivIds[i]).append("<div id='" + imageCheckboxDivIds[i] + "' style='height: 15px; width: auto;'</div>");
              //sets up a checkbox for each image (starts hidden until the edit toggle is on)
              $('#' + imageCheckboxDivIds[i]).append("<input type='checkbox' class='checkbox-image' id='" + imageCheckboxIds[i] + "' name='directory' value='" + videoImages[i][2] + "' style='display: none;'>");
              //sets up each image
              $('#' + imageDivIds[i]).append("<img id='" + imageIds[i] + "' class='video-image' src='" + videoImages[i][2] + "' width='200px' height='120px' style='margin 0px 5px 10px 5px'>");
              // sets up the start and end time for each image
              $('#' + imageDivIds[i]).append("<p>" + videoImages[i][0] + "-" + videoImages[i][1] +"</p>");
            }
              $('#images-container-div').append("</div><div style='clear: both; padding-top: 35px;'>");



            //button that will delete the images
            $('#images-container-div').append("<button id='remove-checked-images' style='display: none;'>Remove selected images</button>");

            $('#images-container-div').append("</div>");

            //if the next picture is 1 second after then only the images will be
            //exchanged and the video will stay minimized
            //example picture ends at 40 seconds and the next image starts at 41
            //$startIndex must be larger than 0 so the animation can start
            var startIndex = 0;
            var endIndex = 0;
            var conIndex = false;
            var currentImg = 0;
            var check = 0;
            $("#video").on(
              "timeupdate",
              function() {
                //check is set to false as it always needs to be set to true for
                //each change in the current time update
                check = false;
                for(var i = 1; i < (videoImages.length + 1); i++) {
                    if (currentTime >= videoImages[i - 1][0] && currentTime <= videoImages[i - 1][1]) {
                        currentImg = i;

                        // enshures that the correct image is shown by cheking if the url of the current img
                        // is equal to the src of the current image
                        if($('#large-video-image').attr('src') != videoImages[currentImg - 1][2]) {
                          $('#large-video-image').attr('src', videoImages[currentImg - 1][2]);

                          $("#" + imageDivIds[currentImg - 1]).css({
                            borderColor: "green"
                          });
                        }
                        //il explain later
                        check = true;
                      }
                    }

                    if(tmpCurrentImg != currentImg) {
                      //removes style from currentImg
                      $("#" + imageDivIds[(tmpCurrentImg - 1)]).css({
                        borderColor: "grey"
                      });
                    }

                    tmpCurrentImg = currentImg;

                    if(check != true) {
                      currentImg = 0;
                    }

                    if(currentImg != 0 && conIndex == false) {
                      //minimize video
                      //console.log(currentImg);
                      $('#video').css({position: 'absolute'})
                      moveVideoWindow("rightbottom");
                      $('#large-video-image').css({display: "inline"});
                      $('#large-video-image').attr('src', videoImages[currentImg - 1][2]);
                      conIndex = true;

                      $("#" + imageDivIds[currentImg - 1]).css({
                        borderColor: "green"
                      });
                    } else if (conIndex == true && currentImg == 0) {
                      $('#large-video-image').css({display: "none"});
                      moveVideoWindow("lefttop");
                      conIndex = false;
                    } else if (conIndex == true && currentImg == 0) {}
          }); //ontime updade


          $('.video-image').click(function() {
            //alert($(this).attr("src"));
            for(var i = 0; i < videoImages.length; i++) {
              if(videoImages[i][2] == $(this).attr("src")) {
                document.getElementById('video').currentTime = videoImages[i][0];
              }
            }
          });

          //toggle edit on click .switch for images
          $('#edit').click(function() {
            if($('#edit').is(':checked')) {
              for(var i = 0; i < imageCheckboxIds.length; i++) {
                $('#' + imageCheckboxIds[i]).css("display" , "block");
                $('#remove-checked-images').css("display" , "block");
              }
            } // on edit checked
            else {
              for(var i = 0; i < imageCheckboxIds.length; i++) {
                $('#' + imageCheckboxIds[i]).hide();
                $('#remove-checked-images').hide();
              }
            } // on edit unchecked
          }); //on edit click

          //removes the images checked from the
          $("#remove-checked-images").click(function() {
            var imgData = new Array();
            var t = 0;
            for(var i = 0; i < imageCheckboxIds.length; i++) {
              if ($('#' + imageCheckboxIds[i]).is(':checked')) {
                imgData[t] = $('#' + imageCheckboxIds[i]).val();
                t++;
              }
            }

            console.log(imgData);

            $.ajax({
              context: this,
              type: 'POST',
              url: 'ajax.php',
              data: {action : 'IMAGE_DELETE', imgData : imgData},
              success: function(response) {
                alert(response);

                for(var i = 0; i < imageCheckboxIds.length; i++) {
                  if ($('#' + imageCheckboxIds[i]).is(':checked')) {
                    $('#' + imageDivIds[i]).css("display" , "none");
                    videoImages.splice(i, 1);
                  }
               } //for loop
             } //on success
           }); //ajax
         }); //on click
     //on success
   }, error :function(response) {
     console.log('fail');
   }
 }); //ajax
}





  //http://stackoverflow.com/questions/6380956/current-duration-time-of-html5-video
  videoPlayer.addEventListener('loadeddata', function() {
    $('#duration').text(videoPlayer.duration);
    $("#video").on(
      "timeupdate",
      function(){
        currentTime = this.currentTime;
        $("#current-time").text(currentTime);
      });
    }, false);
    /*

    ('#newVideoSubmitBtn').click(function(event) {
var title1 = $('#newVideotitleInput').val();								//TODO validation
var description1 = $('#newVideoDescriptionInput').val();

var formData = new FormData();
formData.append('fileToUpload', $('#newVideoFileInput').prop('files')[0]);

for (i = 0; i < $('#subtitlesUploadDiv input').length; i++) {
formData.append('subtitleFiles[' + i + ']', $('#subtitlesUploadDiv input')[i].files[0]);
}

for (i = 0; i < $('#subtitlesUploadDiv select').length; i++) {
formData.append('languages[' + i + ']', $('#subtitlesUploadDiv select').eq(i).val());
}


formData.append('uid', profileId);
formData.append('title', $('#newVideotitleInput').val());
formData.append('description', $('#newVideoDescriptionInput').val());

$("#uploadInProgress").show();


$.ajax({
context: this,
url: 'upload.php',
data : formData,
    processData: false,  // tell jQuery not to process the data
    contentType: false,  // tell jQuery not to set contentType
    type: 'POST',
})
.done(function(data) {
console.log("successnewvideo");
getVideosFromServer(profileId);
console.log(data);
})
.fail(function() {
console.log("errornewvideo");
})
.always(function(data) {
console.log("complete");
$("#uploadInProgress").hide();
});
});

    */

    //Taken from http://jsfiddle.net/rws95a1q/4/
    function moveVideoWindow(moveTo) {
          /*else if (moveTo === "lefttop") {
          videoDivWindow.stop().animate({
          bottom: windowHeight - videoDivHeight
          , right: windowWidth - videoDivWidth
        }, 5000);
      }*/
      if (moveTo === "rightbottom") {
        $('#video').css({position: 'fixed'})
        videoDivWindow.stop().animate({
          bottom: 0,
          right: 0,
          width: 400,
          height: 225
        }, "fast");
      }
      else if (moveTo === "lefttop") {
        videoDivWindow.stop().animate({
          bottom: windowHeight - videoDivHeight,
          right: windowWidth - videoDivWidth,
          width: 960,
          height: 540
        }, "fast", function() {
          videoDivWindow.css({position: 'static'})
        });
        /*else {
        chatWindow.stop().animate({
        bottom: 0
        , right: 0
      }, 2000);
      */
      }
  }
  getOwnerId();


  function addStudentToWatchlist() {
    var vidId = $('#video').attr('data-id');
    var uId = "<?php echo $_SESSION['uid'];?>";
    var ownerId = "<?php echo $videoInterface->getVideoById($id)['owner']; ?>";
    console.log(ownerId + "asd");

     if("<?php echo isset($_SESSION['uid']); ?>") {

        $.ajax({
              context: this,
              url: 'ajax.php',
              data : {action : "ADD_STUDENT_TO_WATCHED_LIST", videoId : vidId, userId : uId, ownerId : ownerId},
              type: 'POST',
              success: function(response){
                  //students = JSON.parse(response);
                  console.log(response);
               }
             });
           }
        }

function getStudentWatchedList() {

    $('#student-list').empty();

    var vidId = $('#video').attr('data-id');
    console.log(vidId = $('#video').attr('data-id'));
      console.log("hahi");

    //console.log("<?php// echo "hahahaha"; echo $user->getAdmin(); ?>");

    if("<?php echo $user->getAdmin(); ?>") {
      $.ajax({
        context: this,
        url: 'ajax.php',
        data : {action: "GET_STUDENT_WATCHED_LIST", videoId : vidId},
        dataType : "json",
        type: 'POST',
        success: function(response){
            //students = JSON.parse(response);
            //console.log(response);

            students = response;

            for(var i = 0; i < students.length; i++) {
              $('#student-list').append("<li>" + students[i] + "</li>");
            }

            console.log(response);
        }
      });
    }
  }


  //Checks if the logged in user is the owner of the video, if this
  //is the case then gives the user control over different functionalities
  //This is done by targeting the different buttons and divs and showing them
  //they are at default sat to hidden
  function getOwnerId() {
    videoId = $('#video').attr('data-id');
    $.ajax({
      context: this,
      url: 'ajax.php',
      data : {action: "GET_VIDEO_BY_ID", id : videoId},
      dataType : "json",
      type: 'POST',
      success: function(response){
        console.log('great success');
          console.log(response);
            console.log(response['owner']);

          if("<?php echo $user->getUID(); ?>" == response['owner']) {
              $('#edit-label').show();
              $('#video-image-upload-div').show();
              $('#subEditCollapseButton').show();
          }

      }
    });
  }
    </script>

</div>
